<?php 
$rol = checkLogin();

$retorno = Array();
$retorno['mensaje'] = "PATATA";
if($rol['rol']!=0)
{
  if(isset($_POST['operacion']))
  {
    $op = $_POST['operacion'];
    if($op==='turnos_por_dia')
    {
      //turnos
      $sql = "select t.id_turno, t.hora_propuesta, u.nombre, u.apellido, u.dni, e.descripcion, e.id_estado ".
                "from turnos t join usuarios u join pacientes p join estados_turno e ".
                "on t.id_paciente=p.id_paciente and p.id_usuario=u.id_usuario and t.id_estado=e.id_estado  ".
                "where fecha='$_POST[fecha]' and id_doctor=$_POST[doctor] and (t.id_estado=1 or t.id_estado=5 or t.id_estado=3) and t.id_especialidad={$_POST['especialidad']} ".
                "order by t.hora_propuesta asc";
      $result = $mysqli->query($sql);
      $turnos = Array();
      while($t = $result->fetch_assoc())
      {
        array_push($turnos, $t);
      }

      //sobreturnos
      $sql =  "select t.id_turno, t.hora_propuesta, u.nombre, u.apellido, u.dni, e.descripcion, e.id_estado, t.sobreturno ".
              "from turnos t join usuarios u join pacientes p join estados_turno e ".
              "on t.id_paciente=p.id_paciente and p.id_usuario=u.id_usuario and t.id_estado=e.id_estado ".
              "where fecha='{$_POST['fecha']}' and id_doctor={$_POST['doctor']} and t.sobreturno=1 and (t.id_estado=1 or t.id_estado=3 or t.id_estado=5 ) and t.id_especialidad={$_POST['especialidad']} ".
              "order by t.hora_propuesta asc";
      $sobreturnos = $mysqli->query($sql);
      $sob = Array();
      while($s = $sobreturnos->fetch_assoc())
      {
        array_push($sob,$s);
      }

      //horarios
      $horarios = $mysqli->query("select h.hora_inicio, h.hora_fin, h.minutos_por_turno,  
      (epd.sobreturnos - (select count(t.id_turno) from turnos t where t.id_doctor={$_POST['doctor']} and t.id_estado=1 and t.sobreturno=1 and t.hora_propuesta=h.hora_fin and t.fecha='{$_POST['fecha']}')) as sobreturnos 
      from horarios h join especialidades_por_doctor epd 
      on h.id_doctor=epd.id_doctor and h.id_especialidad=epd.id_especialidad 
      where epd.id_doctor={$_POST['doctor']} and epd.id_especialidad={$_POST['especialidad']} and h.habilitado=1 and h.id_dia={$_POST['dia']}");
      $schedules = Array();
      while($h = $horarios->fetch_assoc())
      {
        array_push($schedules, $h);
      }
      
      $retorno['sobreturnos']=$sob;
      $retorno['horarios']=$schedules;
      $retorno['turnos']=$turnos;
    }
    else if($op==='cancelar_turno')
    {
      $mysqli->query("UPDATE turnos SET id_estado=2 where id_turno=$_POST[turno]");
      $retorno['borrado'] = ($mysqli->errno != 0)? 0:1;
    }
    else if($op==='cambiar_estado')
    {
      $sql = "UPDATE turnos set id_estado={$_POST['estado']} where id_turno={$_POST['id_turno']}";
      $mysqli->query($sql);
      $retorno['rpta']=($mysqli->errno != 0)? 0:1;
    }
    else if($op==='guardar_turno')
    {
      if( $_POST['secre'] != 0 )
      {
        if($_POST['sobreturno']==1){
          $sql = "INSERT INTO turnos (id_doctor, id_paciente,id_secre, fecha, hora_propuesta, id_estado, id_especialidad, sobreturno) values($_POST[doc],$_POST[paciente],$_POST[secre],'$_POST[fecha]','$_POST[hora]',1,$_POST[especialidad],1)";
        }else{
          $sql = "INSERT INTO turnos (id_doctor, id_paciente,id_secre, fecha, hora_propuesta, id_estado, id_especialidad) values($_POST[doc],$_POST[paciente],$_POST[secre],'$_POST[fecha]','$_POST[hora]',1,$_POST[especialidad])";
        }
      }else{
        $sql = "INSERT INTO turnos (id_doctor, id_paciente, fecha, hora_propuesta, id_estado, id_especialidad) values($_POST[doc],$_POST[paciente],'$_POST[fecha]','$_POST[hora]',1,$_POST[especialidad])";
      }
      $mysqli->query($sql);
      if($mysqli->errno != 0)
      {
        $retorno['rpta']=0;
      }else{
        $retorno['rpta']=1;
      }
    }
    else if($_POST['operacion']==='pacientes')
    {
      $pacientes= $mysqli->query("SELECT p.id_paciente, u.nombre, u.apellido, u.dni from usuarios u join pacientes p on u.id_usuario=p.id_usuario where habilitado=1 and rol=4");
      $pac = Array();
      while($p = $pacientes->fetch_assoc())
      {
        array_push($pac, $p);
      }
      $retorno['pacientes']=$pac;
    }
  }
  echo json_encode($retorno);
}