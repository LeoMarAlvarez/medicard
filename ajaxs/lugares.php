<?php 
$rol = checkLogin();

$retorno = Array();
$retorno['mensaje'] = "PATATA";
if($rol['rol']!=0)
{
  if(isset($_POST['operacion']))
  {
    $op = $_POST['operacion'];
    if($op === 'paises')
    {
      $sql = "SELECT * from paises where nombre like '%$_POST[search]%' order by nombre";
      $result = $mysqli->query($sql);
      $paises = Array();
      while($p = $result->fetch_assoc())
      {
        array_push($paises, $p);
      }
      $retorno['paises']=$paises;
    }
    if($op === 'provincias')
    {
      $sql = "SELECT p.id_provincia, p.nombre, pa.nombre as pais from provincias p join paises pa on p.id_pais=pa.id_pais where p.nombre like '%$_POST[search]%' order by p.nombre";
      $result = $mysqli->query($sql);
      $provincias = Array();
      while($p = $result->fetch_assoc())
      {
        array_push($provincias, $p);
      }
      $retorno['provincias']=$provincias;
    }
    if($op === 'localidades')
    {
      $sql = "SELECT l.id_localidad, l.nombre as localidad, pr.nombre as provincia, pa.nombre as pais ".
                "FROM localidades l join provincias pr join paises pa ".
                "on l.id_provincia=pr.id_provincia and pr.id_pais=pa.id_pais ".
                "where l.nombre like '%{$_POST['search']}%' or pr.nombre like '%{$_POST['search']}%' or pa.nombre like '%{$_POST['search']}%' ".
                "order by l.nombre asc";
      $result = $mysqli->query($sql);
      $localidades = Array();
      while($p = $result->fetch_assoc())
      {
        array_push($localidades, $p);
      }
      $retorno['localidades']=$localidades;
    }
  }
  echo json_encode($retorno);
}