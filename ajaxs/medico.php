<?php 
$rol = checkLogin();

$retorno = Array();
$retorno['mensaje'] = "PATATA";
if($rol['rol']!=0 && $rol['rol']<=4)
{
  if(isset($_POST['operacion']))
  {
    $op = $_POST['operacion'];
    if($op === 'horarios_doctor')
    {
      $sql = "SELECT h.id_hora, h.hora_inicio, h.hora_fin, d.nombre, e.descripcion, h.minutos_por_turno, h.habilitado from horarios h join dias d join especialidades e on h.id_dia=d.id_dia and h.id_especialidad=e.id_especialidad where h.id_doctor=$_POST[doctor] order by h.id_dia, h.hora_inicio";
      $result = $mysqli->query($sql);
      $horarios = Array();
      while($p = $result->fetch_assoc())
      {
        array_push($horarios, $p);
      }
      $retorno['horarios']=$horarios;
    }
    if($op==='medicos_por_especialidad')
    {
      $sql = "SELECT * from usuarios u join especialidades_por_doctor epd on u.id_usuario=epd.id_doctor where epd.habilitado=1 and epd.id_especialidad=".$_POST['especialidad'];
      $resul = $mysqli->query($sql);
      $doctores = Array();
      while($d = $resul->fetch_assoc())
      {
        array_push($doctores, $d);
      }
      $retorno['doctores']=$doctores;
    }
    if($op==='dias_disponibles')
    {
      $d = $mysqli->query("select nombre from horarios h join dias d on h.id_dia=d.id_dia where h.habilitado=1 and h.id_especialidad=".$_POST['especialidad']." and h.id_doctor=".$_POST['doc']." group by nombre order by d.id_dia asc");
      $dias = Array();
      while($di = $d->fetch_assoc())
      {
        array_push($dias,$di);
      }
      $retorno['dias']=$dias;
    }
    if($op==='mis_turnos')
    {
      $turnos = $mysqli->query("select count(t.id_turno) as cantidad, t.fecha
      from turnos t join pacientes p join usuarios u join especialidades e
      on t.id_paciente=p.id_paciente and p.id_usuario=u.id_usuario and e.id_especialidad=t.id_especialidad 
        where t.fecha>='".date('Y-m-d')."' and t.id_doctor={$_SESSION['user']['id_usuario']} and (t.id_estado!=2 or t.id_estado!=4) 
        group by t.fecha
        order by t.fecha, t.hora_propuesta asc");
      $turns = Array();
      while($t = $turnos->fetch_assoc()){
        array_push($turns,$t);
      }
      $retorno['turnos'] = $turns;
    }
  }
  echo json_encode($retorno);
}