<?php 
$rol = checkLogin();

$retorno = Array();
$retorno['mensaje'] = "PATATA";
if($rol['rol']!=0 && $rol['rol']<4)
{
  if(isset($_POST['operacion']))
  {
    $op = $_POST['operacion'];
    if($op==='listar_pacientes')
    {
      $search = $_POST['search'];
      $sql="select u.apellido, u.nombre, u.dni, pa.id_paciente, po.descripcion, os.nombre as obra, u.habilitado ".
            "from pacientes pa join usuarios u join planes_os po join obras_sociales os ".
            "on pa.id_usuario=u.id_usuario and pa.id_plan=po.id_plan and po.id_obra_social=os.id_obra_social ".
            "where u.apellido like '%".$search."%' or u.nombre like '%".$search."%' or po.descripcion like '%".$search."%' or os.nombre like '%".$search."%' ".
            "order by u.apellido, u.nombre asc limit 15";
      $pacs = $mysqli->query($sql);
      $apac = Array();
      while($p = $pacs->fetch_assoc()){
        array_push($apac,$p);
      }
      $retorno['pacientes']=$apac;
    }
  }
  echo json_encode($retorno);
}