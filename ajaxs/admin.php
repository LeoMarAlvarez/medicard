<?php 
$rol = checkLogin();

$retorno = Array();
$retorno['mensaje'] = "PATATA";
if($rol['rol']!=0)
{
  if(isset($_POST['operacion']))
  {
    $op = $_POST['operacion'];
    if($op === 'especialidades')
    {
      $sql = "SELECT * from especialidades where descripcion like '%{$_POST['search']}%' order by descripcion limit 10";
      $result = $mysqli->query($sql);
      $especialidades = Array();
      while($p = $result->fetch_assoc())
      {
        array_push($especialidades, $p);
      }
      $retorno['especialidades']=$especialidades;
    }
    else if($op === 'obrassociales')
    {
      $sql = "SELECT * from obras_sociales where nombre like '%{$_POST['search']}%' order by nombre limit 10";
      $result = $mysqli->query($sql);
      $obrassociales = Array();
      while($p = $result->fetch_assoc())
      {
        array_push($obrassociales, $p);
      }
      $retorno['obrassociales']=$obrassociales;
    }
    else if($op === 'usuarios')
    {
      $sql = "SELECT u.id_usuario, u.dni, u.nombre, u.apellido, u.usuario, u.habilitado, r.nombre as rol from usuarios u join roles r on u.rol = r.id_rol order by u.apellido, u.nombre asc";
      $result = $mysqli->query($sql);
      $usuarios = Array();
      while($p = $result->fetch_assoc())
      {
        array_push($usuarios, $p);
      }
      $retorno['usuarios']=$usuarios;
    }
    else if($op ===  'planes')
    {
      $pls = $mysqli->query("SELECT po.id_plan, po.descripcion, po.habilitado, os.nombre from planes_os po join obras_sociales os on po.id_obra_social=os.id_obra_social where po.descripcion like '%{$_POST['search']}%' or os.nombre like '%{$_POST['search']}%' order by po.descripcion, os.nombre asc");
      $ps = Array();
      while ($p = $pls->fetch_assoc())
      {
        array_push($ps, $p);
      }
      $retorno['planes']=$ps;
    }
    else if($op === 'solucionar_solicitud')
    {
      $hora = new DateTime();
      $solicitu = $mysqli->query("SELECT * from solicitudes where id_solicitud={$_POST['solicitud']}");
      $solicitud = $solicitu->fetch_assoc();
      $mysqli->query("UPDATE solicitudes set estado=0, fecha_resolucion='{$hora->format('Y-m-d H:i:s')}' where id_solicitud={$_POST['solicitud']}");
      if($solicitud['detalle']==='Usuario inactivo')
      {
        $mysqli->query("UPDATE usuarios set habilitado=1 where id_usuario={$solicitud['id_usuario']}");
      }else{
        $mysqli->query("UPDATE usuarios u set u.password=u.dni where u.id_usuario={$solicitud['id_usuario']}");
      }
      $retorno['ok'] = ($mysqli->errno!=0)? 0:1;
    }
  }
  echo json_encode($retorno);
}