let fff, eventos, calendar;

document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    calendar = new FullCalendar.Calendar(calendarEl, {
        locale: 'es',
        timeZone: 'local',
        initialView: 'dayGridMonth'
    });
    getEvents();
    calendar.render();
});

function getEvents() {
    fff = new FormData();
    fff.append('operacion', 'mis_turnos');
    fetch('/ajaxs/medico', {
            method: 'POST',
            body: fff,
        })
        .then(r => r.json())
        .then(r => {
            eventos = new Array();
            let uno = null;
            r.turnos.forEach(e => {
                turns = (Number(e.cantidad) > 1) ? 'Turnos' : 'Turno';
                uno = {
                    title: `${e.cantidad} ${turns}`,
                    start: e.fecha,
                    end: e.fecha
                }
                calendar.addEvent(uno);
            });
        });
}