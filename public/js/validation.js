let form = null;
let span = document.createElement('span');
span.classList.add('text-danger');

document.querySelector("[name='email']").addEventListener('keyup', () => {
    console.log('e');
    let email = document.querySelector("[name='email']").value;
    console.log(email);
    form = new FormData();
    form.append('operacion', 'check_emails');
    form.append('email', email);
    fetch('/ajaxs/generales', { method: 'POST', body: form })
        .then(rpta => rpta.json())
        .then(r => {
            if (r.cantidad > 0) {
                span.classList.remove('d-none');
                span.textContent = "Ya existe un usuario con ese email";
                document.getElementById('formulario').insertBefore(span, document.querySelector("[name='email']").target);
            } else {
                span.classList.add('d-none');
            }
        });
});

document.querySelector("[name='usuario']").addEventListener('keyup', () => {
    let usuario = document.querySelector("[name='usuario']").value;
    console.log(usuario);
    form = new FormData();
    form.append('operacion', 'check_usuarios');
    form.append('usuario', usuario);
    fetch('/ajaxs/generales', { method: 'POST', body: form })
        .then(rpta => rpta.json())
        .then(r => {
            if (r.cantidad > 0) {
                span.classList.remove('d-none');
                span.textContent = "Ya existe un usuario con ese nombre de usuario";
                document.getElementById('formulario').insertBefore(span, document.querySelector("[name='usuario']").target);
            } else {
                span.classList.add('d-none');
            }
        });
});

document.querySelector("[name='dni']").addEventListener('keyup', () => {
    let dni = document.querySelector("[name='dni']").value;
    form = new FormData();
    form.append('operacion', 'check_dni');
    form.append('dni', dni);
    fetch('/ajaxs/generales', { method: 'POST', body: form })
        .then(rpta => rpta.json())
        .then(r => {
            if (r.cantidad > 0) {
                span.classList.remove('d-none');
                span.textContent = "Ya existe un usuario con ese DNI";
                document.getElementById('formulario').insertBefore(span, document.querySelector("[name='dni']").target);
            } else {
                span.classList.add('d-none');
            }
        });
});