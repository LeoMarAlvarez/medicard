let exp = new RegExp("[a-zA-Z]");
let prevDni = null;

function noLetras(dni) {
    if (exp.test(dni)) {
        document.querySelector("[name='dni']").value = prevDni;
        swal({ text: 'El campo dni sólo debe contener números', icon: 'warning', timer: 1500, buttons: false });
    }
}

function pressInput() {
    let dni = document.querySelector("[name='dni']").value;
    noLetras(dni);
}

function prevPressInput() {
    prevDni = document.querySelector("[name='dni']").value;
}