form = null;

function listarTurnos(doctor, fecha, especialidad, esSecre) {
    let listado = document.querySelector('#turnos_body');
    listado.innerHTML = "";
    form = new FormData();
    let fec = new Date(fecha);
    let ini = null;
    let fin = null;
    fec.setDate(fec.getDate() + 1);
    form.append('operacion', 'turnos_por_dia');
    form.append('doctor', doctor);
    form.append('fecha', fecha);
    form.append('especialidad', especialidad);
    form.append('dia', fec.getDay());
    fetch("/ajaxs/turnos", { method: 'POST', body: form })
        .then(r => r.json())
        .then(rpta => {
            let lista = rpta.turnos;
            let horarios = rpta.horarios;
            let mins = null;
            if (lista.length > 0 || horarios.length > 0) {
                let sobreturnos = rpta.sobreturnos;
                let cantSobreturnos = Number(rpta.horarios[0].sobreturnos);
                let d = document.createElement('div');
                d.classList.add("row");
                let contenido = "";
                let minuts = null;
                if (esSecre) {
                    horarios.forEach(h => {
                        contenido += `<div class="col-lg-12 col-md-12 col-sm-12 btn-info" style="margin-bottom:15px; border-radius:10px;"><h5>Turno ${h.hora_inicio} - ${h.hora_fin}</h5></div>`;
                        mins = h.minutos_por_turno;
                        ini = new Date(fec);
                        fin = new Date(fec);
                        ini.setHours(h.hora_inicio.slice(0, 2));
                        ini.setMinutes(h.hora_inicio.slice(3, -3));
                        fin.setHours(h.hora_fin.slice(0, 2));
                        fin.setMinutes(h.hora_fin.slice(3, -3));
                        let contenidoo = null;
                        while (ini < fin) {
                            if (ini.getMinutes() == 0) {
                                minuts = `00`;
                            } else if (ini.getMinutes() < 10) {
                                minuts = `0${ini.getMinutes()}`;
                            } else {
                                minuts = ini.getMinutes();
                            }
                            if (ini.getHours() < 10) {
                                hours = `0${ini.getHours()}`;
                            } else if (ini.getHours() == 0) {
                                hours = `00`;
                            } else {
                                hours = ini.getHours();
                            }
                            let hora = null;
                            minutos = null;
                            contenidoo = `
                                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 turno">
                                <button class="btn btn-success btnTurno" data-turno="${hours}:${minuts}" data-toggle="modal" data-target="#modal-default">
                                    <h3 data-turno="${hours}:${minuts}"><i class="fa fa-table"></i> ${hours}:${minuts}</h3>
                                    <br>
                                    <p data-turno="${hours}:${minuts}">Estado: Disponible</p>
                                </button>
                                </div>
                            `;
                            if (lista.length > 0) {
                                let dis = null;
                                lista.forEach(t => {
                                    hora = t.hora_propuesta.slice(0, 2);
                                    minutos = t.hora_propuesta.slice(3, -3);
                                    if (ini.getHours() == hora && ini.getMinutes() == minutos && t.id_estado != 2) {
                                        dis = (t.id_estado != 1) ? 'disabled' : '';
                                        contenidoo = `
                                      <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 turno" data-toggle="tooltip" title="${t.apellido} ${t.nombre}">
                                          <button disabled class="btn btn-warning">
                                            <h3><i class="fa fa-table"></i> ${hours}:${minuts}</h3>
                                            <p>Estado: No disponible</p>
                                            <a href="#" onclick="cancelarTurno(${t.id_turno})" class="btn btn-xs btn-danger ${dis}" title="Cancelar"><i class="fa fa-times"></i></a>
                                          </button>
                                      </div>
                                    `;
                                    }
                                });
                                contenido += contenidoo;
                            } else {
                                contenido += `
                              <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 turno">
                                <button class="btn btn-success btnTurno" data-turno="${hours}:${minuts}" data-toggle="modal" data-target="#modal-default">
                                  <h3 data-turno="${hours}:${minuts}"><i class="fa fa-table"></i> ${hours}:${minuts}</h3>
                                  <br>
                                  <p data-turno="${hours}:${minuts}">Estado: Disponible</p>
                                </button>
                              </div>
                            `;
                            }

                            ini.setMinutes(ini.getMinutes() + Number(mins));
                        }
                        if (sobreturnos.length > 0) {
                            sobreturnos.forEach(t => {
                                if (ini.getMinutes() == 0) {
                                    minuts = `00`;
                                } else {
                                    minuts = ini.getMinutes();
                                }
                                if (ini.getHours() < 10) {
                                    hours = `0${ini.getHours()}`;
                                } else {
                                    hours = ini.getHours();
                                }
                                if (t.hora_propuesta === `${hours}:${minuts}:00`) {
                                    t.id_estado = (t.id_estado == 1) ? "" : "disabled";
                                    contenido += `
                                    <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 turno" data-toggle="tooltip" title="${t.apellido} ${t.nombre}">
                                        <button disabled class="btn btn-warning">
                                        <p class="badge-info">(Sobreturno)</p>
                                        <h3><i class="fa fa-table"></i> ${hours}:${minuts}</h3>
                                        <p>Estado: No disponible</p>
                                        <a href="#" onclick="cancelarTurno(${t.id_turno})" class="btn btn-xs btn-danger ${t.id_estado}" title="Cancelar"><i class="fa fa-times"></i></a>
                                        </button>
                                    </div>
                                `;
                                }
                            });
                            if (cantSobreturnos > 0) {
                                contenido += `
                                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 turno sobreturno">
                                    <button class="btn btn-primary btnTurno" data-turno="${h.hora_fin}" data-sobreturno="1" data-toggle="modal" data-target="#modal-default">
                                    <h3 data-turno="${h.hora_fin}" data-sobreturno="1"><i class="fa fa-table"></i> ${h.hora_fin}</h3>
                                    <br>
                                    <p data-turno="${h.hora_fin}" data-sobreturno="1">Sobreturno (${h.sobreturnos})</p>
                                    </button>
                                </div>
                                `;
                            } else {
                                contenido += `
                                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 turno sobreturno">
                                    <button class="btn btn-default btnTurno" disabled data-turno="${h.hora_fin}" data-sobreturno="1" data-toggle="modal" data-target="#modal-default">
                                    <h3 data-turno="${h.hora_fin}" data-sobreturno="1"><i class="fa fa-table"></i> ${h.hora_fin}</h3>
                                    <br>
                                    <p data-turno="${h.hora_fin}" data-sobreturno="1">Sobreturnos completos</p>
                                    </button>
                                </div>
                                `;
                            }
                        } else {
                            if (cantSobreturnos > 0) {
                                contenido += `
                                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 turno sobreturno">
                                    <button class="btn btn-primary btnTurno" data-turno="${h.hora_fin}" data-sobreturno="1" data-toggle="modal" data-target="#modal-default">
                                    <h3 data-turno="${h.hora_fin}" data-sobreturno="1"><i class="fa fa-table"></i> ${h.hora_fin}</h3>
                                    <br>
                                    <p data-turno="${h.hora_fin}" data-sobreturno="1">Sobreturno (${cantSobreturnos})</p>
                                    </button>
                                </div>
                                `;
                            } else {
                                contenido += `
                                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 turno sobreturno">
                                    <button class="btn btn-default btnTurno" disabled data-turno="${h.hora_fin}" data-sobreturno="1" data-toggle="modal" data-target="#modal-default">
                                    <h3 data-turno="${h.hora_fin}" data-sobreturno="1"><i class="fa fa-table"></i> ${h.hora_fin}</h3>
                                    <br>
                                    <p data-turno="${h.hora_fin}" data-sobreturno="1">Sobreturnos completos</p>
                                    </button>
                                </div>
                                `;
                            }
                        }

                    });
                } else {
                    horarios.forEach(h => {
                        contenido += `<div class="col-lg-12 col-md-12 col-sm-12 btn-info" style="margin-bottom:15px; border-radius:10px;" ><h5>Turno ${h.hora_inicio} - ${h.hora_fin}</h5></div>`;
                        mins = h.minutos_por_turno;
                        ini = new Date(fec);
                        fin = new Date(fec);
                        ini.setHours(h.hora_inicio.slice(0, 2));
                        ini.setMinutes(h.hora_inicio.slice(3, -3));
                        fin.setHours(h.hora_fin.slice(0, 2));
                        fin.setMinutes(h.hora_fin.slice(3, -3));
                        let contenidoo = null;
                        while (ini < fin) {
                            if (ini.getMinutes() == 0) {
                                minuts = `00`;
                            } else {
                                minuts = ini.getMinutes();
                            }
                            if (ini.getHours() < 10) {
                                hours = `0${ini.getHours()}`;
                            } else {
                                hours = ini.getHours();
                            }
                            let hora = null,
                                minutos = null,
                                contenidoo = `
                                    <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 turno">
                                    <button class="btn btn-success btnTurno" data-turno="${hours}:${minuts}" data-toggle="modal" data-target="#modal-default">
                                        <h3 data-turno="${hours}:${minuts}"><i class="fa fa-table"></i> ${hours}:${minuts}</h3>
                                        <br>
                                        <p data-turno="${hours}:${minuts}">Estado: Disponible</p>
                                    </button>
                                    </div>
                                `;
                            if (lista.length > 0) {
                                lista.forEach(t => {
                                    hora = t.hora_propuesta.slice(0, 2);
                                    minutos = t.hora_propuesta.slice(3, -3);
                                    if (ini.getHours() == hora && ini.getMinutes() == minutos && t.id_estado != 2) {
                                        contenidoo = `
                                      <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 turno">
                                          <button disabled class="btn btn-warning">
                                            <h3><i class="fa fa-table"></i> ${hours}:${minuts}</h3>
                                            <p>Reservado</p>
                                          </button>
                                      </div>
                                    `;
                                    }
                                });
                                contenido += contenidoo;
                            } else {
                                contenido += `
                              <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 turno">
                                <button class="btn btn-success btnTurno" data-turno="${hours}:${minuts}" data-toggle="modal" data-target="#modal-default">
                                  <h3 data-turno="${hours}:${minuts}"><i class="fa fa-table"></i> ${hours}:${minuts}</h3>
                                  <br>
                                  <p data-turno="${hours}:${minuts}">Estado: Disponible</p>
                                </button>
                              </div>
                            `;
                            }

                            ini.setMinutes(ini.getMinutes() + Number(mins));
                        }
                    });
                }

                d.innerHTML = contenido;
                listado.append(d);
            } else {
                listado.innerHTML = `
                <div class="m-5 alert alert-danger">El doctor no atiende ese día!!</div>
              `;
            }
        });
}

function cargarTurno(id_paciente, id_doctor, hora, fecha, secre = null, especialidad, sobreturno) {
    let s = (secre != null) ? Number(secre) : 0;
    let esSecre = (secre != null) ? true : false;
    form = new FormData();
    form.append('operacion', 'guardar_turno');
    form.append('paciente', id_paciente);
    form.append('doc', id_doctor);
    form.append('hora', hora);
    form.append('fecha', fecha);
    form.append('secre', s);
    form.append('especialidad', especialidad);
    form.append('sobreturno', sobreturno);
    fetch('/ajaxs/turnos', { method: 'POST', body: form })
        .then(r => r.json())
        .then(r => {
            if (Number(r.rpta) == 1) {
                swal({
                    title: 'Guardado!',
                    text: `Se guardó correctamente`,
                    icon: 'success'
                }).then(() => {
                    if (!esSecre) {
                        window.location.href = "/paciente/proximos-turnos";
                    }
                });
            } else {
                swal({
                    title: 'Ups!',
                    text: 'Tuvimos un problema al intentar guardar el turno',
                    icon: 'warning'
                });
            }
            if (esSecre) {
                listarTurnos(id_doctor, fecha, especialidad, esSecre);
            }
        });
}

function cancelarTurno(id_turno) {
    swal({
            title: "Estás segur@?",
            text: "Confirmanos la cancelación del Turno",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "No",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "Si",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            },
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                form = new FormData();
                form.append('operacion', 'cancelar_turno');
                form.append('turno', id_turno);

                fetch("/ajaxs/turnos", { method: "POST", body: form })
                    .then(r => r.json())
                    .then(rpta => {
                        if (Number(rpta.borrado) != 0) {
                            swal("Cambios guardados!", { buttons: false, timer: 2000, icon: 'success' });
                        } else {
                            swal({ title: 'Ups!', text: "No pudimos cancelar el turno. Contáctate con el administrador del sitio", icon: 'error' });
                        }
                        listarTurnos(document.querySelector('#doctor').value, document.querySelector('#fecha').value, document.querySelector('#especialidad').value, true);
                    });
            }
        });
}

function cancelTurn(id_turno) {
    swal({
            title: "Estás segur@?",
            text: "Confirmanos la cancelación del Turno",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "No",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "Si",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            },
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                form = new FormData();
                form.append('operacion', 'cancelar_turno');
                form.append('turno', id_turno);

                fetch("/ajaxs/turnos", { method: "POST", body: form })
                    .then(r => r.json())
                    .then(rpta => {
                        if (Number(rpta.borrado) != 0) {
                            swal("Cambios guardados!", { buttons: false, timer: 2000, icon: 'success' });
                            window.location.reload();
                        } else {
                            swal({ title: 'Ups!', text: "No pudimos cancelar el turno. Contáctate con el administrador del sitio", icon: 'error' });
                        }
                        listarTurnos(document.querySelector('#doctor').value, document.querySelector('#fecha').value, document.querySelector('#especialidad').value);
                    });
            }
        });
}

function cambiarEstadoTurno(id_turno, estado) {
    form = new FormData();
    form.append('operacion', 'cambiar_estado');
    form.append('id_turno', id_turno);
    form.append('estado', estado);
    fetch('/ajaxs/turnos', {
            method: 'POST',
            body: form
        })
        .then(r => r.json())
        .then(r => {
            if (Number(r.rpta) != 0) {
                swal({ text: "Cambios guardados!", icon: 'success', buttons: false, timer: 2000 })
                    .then(() => {
                        window.location.reload();
                    });
            } else {
                swal({ title: 'Ups!', text: "No pudimos realizar los cambios.", icon: 'error' })
                    .then(() => {
                        window.location.reload();
                    });
            }
        });
}

function cargarPacientes() {
    form = new FormData();
    form.append('operacion', 'pacientes');
    fetch('/ajaxs/turnos', { method: 'POST', body: form })
        .then(r => r.json())
        .then(r => {
            let campo = document.querySelector("#paciente");
            let filas = `<option value="" selected disabled>Seleccione...</option>`;
            r.pacientes.forEach(p => {
                filas += `
                    <option value="${p.id_paciente}">${p.apellido} ${p.nombre} - ${p.dni}</option>
                `;
            });
            campo.innerHTML = filas;
        });
}

function showDiasDoc(id = null) {
    let div = document.querySelector('#dias_doctor');
    if (id != null) {
        form = new FormData();
        form.append('operacion', 'dias_disponibles');
        form.append('doc', id);
        form.append('especialidad', document.querySelector('#especialidad').value);
        fetch('/ajaxs/medico', { method: 'POST', body: form })
            .then(r => r.json())
            .then(r => {
                let fila = "";
                if (r.dias.length > 0) {
                    r.dias.forEach(d => {
                        fila += `<label class="label label-info h6">${d.nombre}</label>&nbsp;`;
                    });
                } else {
                    fila = "<label class='text-danger text-white text-md-left'>El doctor no tiene horarios cargados</label>";
                    document.querySelector('#fecha').disabled = true;
                }
                div.innerHTML = fila;
            });
    } else {
        div.innerHTML = "";
    }
}