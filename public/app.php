<?php
$url = Array();
if(isset($_GET['url'])){
  
  include "../config/conexion.php";
  include "../config/functions.php";

  $url = explode('/',$_GET['url']);
  
  if($url[0]==='login' || $url[0]==='registro' || $url[0]==='restablecer' || $url[0]==='mi-perfil')
  {
    $archivo = "../pages/$url[0].php";
    if(file_exists($archivo))
    {
      require_once $archivo;
    }else{
      require_once "../pages/error.php";
    }
  }else if($url[0]==='logout'){
    logout();
  }else if($url[0]==='ajaxs'){
    $archivo = "../ajaxs/$url[1].php";
    require_once $archivo;
  }else if( count($url)>1 ){
    $archivo = "../modulos/$url[0]/$url[1].php";
    if(file_exists($archivo))
    {
      require_once $archivo;
    }else{
      require_once "../pages/error.php";
    }
  }else{
    require_once "../pages/error.php";
  }
}