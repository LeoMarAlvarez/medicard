<header class="main-header" style="background-color: <?php ?>;">
  <!-- Logo -->
  <a href="<?php echo $rol['inicio']; ?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">M</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">Medicard</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="/images/icons/<?php echo $_SESSION['user']['rol'].'_'.$_SESSION['user']['sexo'].'.png';?>" class="user-image" alt="User Image">
            <span class="hidden-xs"><?php echo $_SESSION['user']['nombre']; ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="/images/icons/<?php echo $_SESSION['user']['rol'].'_'.$_SESSION['user']['sexo'].'.png';?>" class="img-circle" alt="User Image">

              <p>
                <?php echo $_SESSION['user']['nombre'].' '.$_SESSION['user']['apellido']; ?>
              </p>
            </li>
            <li class="user-footer">
              <a href="/mi-perfil" class="btn btn-block btn-default" style="color:#000;">Editar</a>
              <a href="/logout" class="btn btn-block btn-danger" style="color:#000;">Salir</a>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
      </ul>
    </div>
  </nav>
</header>