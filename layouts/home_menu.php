<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <!-- <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i><span>Documentation</span></a></li> -->
        <?php if($rol['rol'] == 1) { 
          //Usuario Rol Administrador
        ?>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-globe"></i> <span>Lugares</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="/administrador/paises"><img width="20px" src="/images/icons/pais.png"> Países</a></li>
              <li><a href="/administrador/provincias"><img width="20px" src="/images/icons/prov.png"> Provincias</a></li>
              <li><a href="/administrador/localidades"><img width="20px" src="/images/icons/loc.png"> Localidades</a></li>
            </ul>
          </li>
          <li><a href="/administrador/especialidades"><i class="fa fa-user-md"></i><span>Especialidades</span></a></li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-bookmark"></i> <span>Prepagas</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="/administrador/obras-sociales"><i class="fa fa-h-square"></i> Obras Sociales</a></li>
              <li><a href="/administrador/planes-obras-sociales"><i class="fa fa-medkit"></i> Planes</a></li>
            </ul>
          </li>
          <li><a href="/administrador/usuarios"><i class="fa fa-users"></i><span>Usuarios</span></a></li>

        <?php }else if($rol['rol']==2){ 
          //Usuario Rol Médico
        ?>
          <!-- <li><a href="/medico/hoy"><i class="fa fa-list"></i><span>Turnos del día</span></a></li> -->
          <li><a href="/medico/mis-especialidades"><i class="fa fa-user-md"></i><span>Mis Especialidades</span></a></li>
          <li><a href="/medico/mis-horarios"><i class="fa fa-hourglass-2"></i><span>Mis Horarios</span></a></li>
          <li><a href="/medico/mi-historial"><i class="fa fa-history"></i><span>Historial</span></a></li>
          <li><a href="/medico/agenda"><i class="fa fa-calendar"></i><span>Mi Agenda</span></a></li>
        <?php }else if($rol['rol']==3){ 
          //Usuario Rol Secretaria
        ?>
          <li><a href="/secretaria/pacientes"><i class="fa fa-users"></i><span>Pacientes</span></a></li>
          <li><a href="/secretaria/turnos"><i class="fa fa-calendar"></i><span>Turnos</span></a></li>
          <li><a href="/secretaria/solicitud"><i class="fa fa-hashtag"></i><span>Ayuda al paciente</span></a></li>
        <?php }else if($rol['rol']==4){ 
          //Usuario Rol Paciente
          $ant = $mysqli->query("SELECT * from antecedentes a where a.id_paciente={$_SESSION['paciente']['id_paciente']}");
          $cantidad = $ant->num_rows;
        ?>
          <li><a href="/paciente/mis-turnos"><i class="fa fa-history"></i><span>Historial</span></a></li>
          <li><a href="/paciente/antecedente"><i class="fa fa-list"></i><span>Antecedentes</span></a></li>
          <li><a href="/paciente/proximos-turnos"><i class="fa fa-calendar-times-o"></i><span>Próximos Turnos</span></a></li>
          <?php if($cantidad>0){?><li><a href="/paciente/nuevo-turno"><i class="fa fa-plus-square-o"></i><span>Nuevo Turno</span></a></li><?php } ?>
                   
        <?php } ?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>