<?php
  date_default_timezone_set('America/Argentina/Buenos_Aires');
  $rol = checkLogin();
  $class = $rol['class'];
  // echo "<script>alert('$rol')</script>";
  if($rol['rol']==0)
  {
    logout();
    echo "<script>window.location.href='/'</script>";
  }
  permisos($rol['rol']);

  function dia($d){
    return date('d-m-Y', strtotime($d));
  }
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Medicard</title>
  <link rel="icon" type="/image/png" href="/images/logo.png" />
  <!-- SWITCH VER
  <link rel="stylesheet" href="/plugins/adminlte/dist/css/skins/estilos.css">
  -->
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/plugins/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/plugins/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/plugins/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/plugins/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="/plugins/adminlte/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <!-- <link rel="stylesheet" href="/plugins/adminlte/bower_components/morris.js/morris.css"> -->
  <!-- jvectormap -->
  <link rel="stylesheet" href="/plugins/adminlte/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="/plugins/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="/plugins/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Datatable -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="/plugins/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="/plugins/adminlte/bower_components/select2/dist/css/select2.min.css">
  <!-- Estilos -->
  <link rel="stylesheet" href="/css/estilos.css">
  <!-- SweetAlert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
    let lang = {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "copy": "Copiar",
            "colvis": "Visibilidad"
        }
    }

    function showMsg(type, msg,ruta)
    {
      let paramSwal = null;
      if(type==='error')
      {
        paramSwal={
          title:msg,
          text:'Si el problema persiste, contáctate con el administrador del sitio',
          icon:type
        }        
      }else{
        paramSwal={
          text:msg,
          icon:type,
          buttons:false,
          timer:3500
        }
      }
      swal(paramSwal)
      .then(()=>{
        window.location.href=`${ruta}`;
      });
    }
  </script>
</head>
<body class="<?php echo $class; ?> sidebar-mini" style="height: auto;">
  <div class="wrapper">
    <?php 
      include "home_nav.php";
      include "home_menu.php";
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    