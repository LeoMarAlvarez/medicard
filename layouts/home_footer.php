  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2019-2020 <a href=> Medicard S.A.</a></strong> Todos los derechos reservados.
  </footer>

<!-- jQuery 3 -->
<script src="/plugins/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/plugins/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="/plugins/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="/plugins/adminlte/bower_components/raphael/raphael.min.js"></script>
<!-- <script src="/plugins/adminlte/bower_components/morris.js/morris.min.js"></script> -->
<!-- Sparkline -->
<script src="/plugins/adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/plugins/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/plugins/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/plugins/adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="/plugins/adminlte/bower_components/moment/min/moment.min.js"></script>
<script src="/plugins/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/plugins/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/plugins/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/plugins/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/plugins/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/plugins/adminlte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="/plugins/adminlte/dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="/plugins/adminlte/dist/js/demo.js"></script>
<!-- Select2 -->
<script src="/plugins/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>
<!-- Function para mostrar el box que debería estár activo -->
<script>
  $(document).ready(()=>{
    $('.select2').select2();

    <?php if(isset($url[2])){ ?>
      document.querySelector('#lista').classList.add('collapsed-box');
      document.querySelector('#lista .box-header .box-tools button i').classList.remove('fa-minus');
      document.querySelector('#lista .box-header .box-tools button i').classList.add('fa-plus');
    <?php }else{ ?>
      document.querySelector('#formulario').classList.add('collapsed-box');
      document.querySelector('#formulario .box-header .box-tools button i').classList.remove('fa-minus');
      document.querySelector('#formulario .box-header .box-tools button i').classList.add('fa-plus');
    <?php } ?>
  });

  function eliminar(columna,id,tabla)
  {
    swal({
      title: "Estás segur@?",
      text: "Por favor! confirmanos que deseas eliminar el registro",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        let frm = new FormData();
        frm.append('operacion','borrar_registro');
        frm.append('campo',columna);
        frm.append('id',id);
        frm.append('tabla',tabla);

        fetch("/ajaxs/generales",{method:"POST",body:frm})
        .then(r=>r.json())
        .then(rpta=>{
          if(rpta.borrado)
          {
            swal("Cambios guardados!",{buttons:false,timer:2000,icon:'success'});
          }else{
            swal({title:'Ups!',text:"No pudimos borrar el elemento seleccionado. Contáctate con el admin del sitio",icon:'error'});
          }
          // cargarLista('');
          window.location.reload();
        });
      }
    });
  }
</script>  

</body>
</html>