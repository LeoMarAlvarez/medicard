<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Medicard</title>
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <!-- links -->
    <link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.css">

    <!-- Font Awesome -->
  	<link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="/plugins/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  	<!-- icheck bootstrap -->
	 <link rel="stylesheet" href="/plugins/adminlte/plugins/iCheck/all.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="/plugins/adminlte/dist/css/AdminLTE.min.css">
  	<!-- Google Font: Source Sans Pro -->
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
		<!-- SweetAlert -->
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/css/estilos.css">
  </head>
<body>
  
  