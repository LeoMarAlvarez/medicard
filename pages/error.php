<?php 
  include "../layouts/page_head.php"; 

  $rol = checkLogin();
?>

<div class="container text-center align-content-center justify-content-center" >
  <div class="row m-5 p-5 text-center border-dark border rounded" style="background:white !important">
    <div class="col-4 alert-warning rounded-circle">
      <img src="/images/icons/404.png" class="img-fluid">
    </div>
    <div class="col-8 text-center border-dark">
      <h3>Ups!!</h3>
      <p>Es posible que estemos refaccionando este enlace. Prueba más tarde, o contáctate con el administrador del sitio</p>
      <br>
      <br>
      <br>
      <p><a href="<?php echo (isset($_SESSION['user']))? $rol['inicio']:'/'; ?>" class="nav-link">Volver</a></p>
    </div>
  </div>
</div>

<?php include "../layouts/page_foot.php" ?>