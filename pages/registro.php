<?php 
  include "../layouts/page_head.php" ;
  $hoy = new DateTime();
  $hoy->format('Y-m-d');
  if(isset($_POST['action']))
  {
    $countUsers = $mysqli->query("SELECT * from usuarios u where u.usuario='{$_POST['usuario']}' or u.dni='{$_POST['dni']}'");
    $countPac = $mysqli->query("SELECT * from pacientes p where p.email='{$_POST['email']}'");
    if($countUsers->num_rows > 0 || $countPac->num_rows>0)
    {
      echo "<script>swal({text:'Alguno de los datos ingresados ya corresponden a un usuario registrado',icon:'warning'})</script>";
    }else{
      $mysqli->query("INSERT into usuarios (nombre, dni, apellido, rol, usuario, password, sexo) values('{$_POST['nombre']}','{$_POST['dni']}','{$_POST['apellido']}', 4,'{$_POST['usuario']}','{$_POST['password']}','{$_POST['sexo']}')");
      $id = $mysqli->insert_id;
      $mysqli->query("INSERT into pacientes (id_usuario, email, fecha_nacimiento) values({$id}, '{$_POST['email']}', '{$_POST['fecha']}')");
      $usuario = $mysqli->query("SELECT * from usuarios where id_usuario={$id}");
      $user = $usuario->fetch_assoc();
      login(['usuario'=>$user['usuario'],'pass'=>$user['password']]);

      if($mysqli->errno != 0){
        echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/paciente/registro');</script>";
      }
    }
  }

?>

<!-- Main content -->
<section class="content">
  <!-- Form Usuario -->
  <div class="text-center justify-content-center align-items-center d-flex">
    <div class="card m-4">
      <div class="card-header text-center">
        <div class="login-logo">
          <h3><img width="7.5%" src="/images/logo.png"> Nuevo Usuario</h3>
        </div>
      </div>
      <div class="card">
        <div class="card-body login-card-body">
          <div class="box-body" id="formulario">
            <form action="" method="post" class="text-center" id="form">
              <input type="hidden" name="action" value="ok">
              <div class="row form-group text-left">
                <div class="col-12 col-md-6">
                  <label>Nombre</label>
                  <input type="text" name="nombre" class="form-control" required>
                </div>
                <div class="col-12 col-md-6">
                  <label>Apellido</label>
                  <input type="text" name="apellido" class="form-control" required>
                </div>
              </div>  
              <div class="row form-group text-left">
                <div class="col-12 col-md-6">
                  <label>Usuario</label>
                  <input type="text" name="usuario" class="form-control" required>
                </div>
                <div class="col-12 col-md-6">
                  <label>Contraseña</label>
                  <input type="password" name="password" class="form-control" required>
                </div>
              </div>
              <div class="row form-group text-left">
                <div class="col-12 col-md-6">
                  <label>DNI</label>
                  <input type="text" name="dni" onkeyup="pressInput()" onkeypress="prevPressInput()" class="form-control" required minlength="8">
                </div>
                <div class="col-12 col-md-6">
                  <label for="">Sexo</label>
                  <select name="sexo" class="form-control" required>
                    <option value="" selected disabled>Seleccione...</option>
                    <option value="h">Hombre</option>
                    <option value="m">Mujer</option>
                  </select>
                </div>
              </div>
              <div class="row form-group text-left">
                <div class="col-12 col-md-6">
                  <label for="">E-mail</label>
                  <input type="email" name="email" class="form-control" required>
                </div>
                <div class="col-12 col-md-6">
                  <label for="">Fecha de Nacimiento</label>
                  <input type="date" name="fecha" max="$hoy" class="form-control" required>
                </div>
              </div>
              <br>
              <div class="form-group text-center">
                <input type="submit" value="Enviar" class="btn btn-primary btn-block">
              </div>
            </form>
            <p class="mt-3 mb-1 text-right">
              <a href="/login">Ya tengo un Usuario</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
          
  

<?php include "../layouts/page_foot.php" ?>
<script src="/js/noLetters.js"></script>
<script src="/js/validation.js"></script>