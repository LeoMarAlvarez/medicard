<?php 
  include "../layouts/page_head.php" ;
  if(isset($_POST['email']))
  {
    $first = $mysqli->query("SELECT u.id_usuario, p.email, u.habilitado from usuarios u join pacientes p on p.id_usuario=u.id_usuario where p.email='{$_POST['email']}'");
    if($first->num_rows<1)
    {
      echo "<script>swal({text:'No existe una cuenta con ese mail.',icon:'warning'})</script>";
    }else{
      $first = $first->fetch_assoc();
      $fecha = new DateTime();
      $fecha = $fecha->format("Y-m-d H:i:s");
      $detalle= ($first['habilitado']==1)? 'Restablecimiento de contraseña':'Usuario inactivo';
      $mysqli->query("INSERT INTO solicitudes (detalle, id_usuario, fecha_solicitud) values('{$detalle}', {$first['id_usuario']}, '{$fecha}')");
      
      if($mysqli->errno!=0){
        echo "<script>swal({text:'Ups!! ocurrió un error. Intenta nuevamente más tarde',icon:'warning'});</script>";
      }else{
        $message = ($first['habilitado']==1)? 'Solicitud enviada exitosamente. Se le enviará una nueva contraseña a la brevedad':'Solicitud enviada exitosamente. Se le notificará cuando su cuenta se encuentre activa nuevamente';
        echo "<script>swal({text:'{$message}',icon:'success'});</script>";
      }
    }
  }
?>

<!-- Main content -->
<section class="content">
  <!-- Form Usuario -->
  <div class="text-center justify-content-center align-items-center d-flex">
    <div class="card" style="width:500px;">   
      <div class="card-header login-card-header">
        <div class="login-logo">
          <h3><img width="7.5%" src="/images/logo.png"><b>M</b>edicard</h3>
        </div>
      </div> 
      <div class="card">
        <div class="card-body login-card-body">
          <p class="login-box-msg">¿Ha olvidado su contraseña o su cuenta se encuentra inhabilitada?<br>Aqui puede recuperarla.</p>
          <form action="" method="post" id="formulario">
            <div class="input-group mb-3">
              <input type="email" class="form-control" name="email" placeholder="Correo Electronico" required>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fa fa-envelope"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block">Solicitar nueva contraseña</button>
              </div>
            </div>
          </form>
          <p class="mt-3 mb-1 text-right">
            <a href="/login">Iniciar Sesión</a>
          </p>
          <p class="mb-0 text-right">
            <a href="/registro">Registrarse</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include "../layouts/page_foot.php" ?>