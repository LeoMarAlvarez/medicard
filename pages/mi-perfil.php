<?php 
  include "../layouts/home_header.php" ;
  if($rol['rol']==4)
  {
    $paises = $mysqli->query("SELECT id_pais, nombre from paises order by nombre asc");
    $localidades = $mysqli->query("SELECT l.id_localidad, l.nombre, p.nombre as provincia from localidades l join provincias p on l.id_provincia=p.id_provincia order by l.nombre asc");
    $paciente = $mysqli->query("SELECT * from pacientes where id_usuario={$_SESSION['user']['id_usuario']}");
    $paciente = $paciente->fetch_assoc();
    $obras = $mysqli->query("SELECT * from obras_sociales order by nombre asc");
    if($paciente['id_plan']!=null){
      $plan = $mysqli->query("SELECT * from planes_os where id_plan={$paciente['id_plan']}");
      $plan = $plan->fetch_assoc();
      $planes = $mysqli->query("SELECT * from planes_os where id_obra_social={$plan['id_obra_social']} and habilitado=1 order by descripcion asc");
    }
  }


  if(isset($_POST['action']))
  {
    //guardo todo
    if($rol['rol']==4){
      
      $mysqli->query("UPDATE pacientes set fecha_nacimiento='{$_POST['fecha_nacimiento']}', id_nacionalidad={$_POST['nacionalidad']}, direccion='{$_POST['direccion']}', email='{$_POST['email']}', telefono='{$_POST['telefono']}'  where id_paciente={$paciente['id_paciente']}");
      if($_POST['localidad']!=null)
      {
        $mysqli->query("UPDATE pacientes set id_localidad={$_POST['localidad']} where id_paciente={$paciente['id_paciente']}");
      }else if($_POST['plan']!=null)
      {
        $mysqli->query("UPDATE pacientes set id_plan={$_POST['plan']} where id_paciente={$paciente['id_paciente']}");
      }else if($_POST['socio_obra_social']!=null)
      {
        $mysqli->query("UPDATE pacientes set socio_obra_social='{$_POST['socio_obra_social']}' where id_paciente={$paciente['id_paciente']}");
      }
    }

    $mysqli->query("UPDATE usuarios set password='{$_POST['password']}' where id_usuario={$_SESSION['user']['id_usuario']}");
    if($mysqli->errno!=0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','{$rol['inicio']}');</script>";
    }else{
      $_SESSION['user']['password'] = $_POST['password'];
      echo "<script>showMsg('success','Se han guardado los cambios','{$rol['inicio']}');</script>";
    }
  }
?>

<section class="content-header">
  <h1>
    Modificar Perfil
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="box box-default">
    <div class="box-body" id="formulario">
      <form action="" method="post" class="text-center" id="form">
        <input type="hidden" name="action" value="ok">
        <div class="row form-group text-left">
          <label class="col-12 col-md-2 text-right">Nueva Contraseña</label>
          <div class="col-12 col-md-4">
            <input type="password" name="password" class="form-control" value="<?php echo $_SESSION['user']['password'] ?>" required>
          </div>
          <label class="col-12 col-md-2 text-right" id="repeatPass">Repetir Contraseña</label>
          <div class="col-12 col-md-4">
            <input type="password" name="password_confirm" class="form-control" value="<?php echo $_SESSION['user']['password'] ?>" required>
          </div>
        </div>
        <?php if($rol['rol']==4){ ?>
          <div class="row form-group text-left">
            <label class="col-12 col-md-2 text-right">E-mail</label>
            <div class="col-12 col-md-4">
              <input type="email" name="email" class="form-control" value="<?php echo ($paciente['email']!=null)? $paciente['email']:''; ?>">
            </div>
            <label class="col-12 col-md-2 text-right">Teléfono</label>
            <div class="col-12 col-md-4">
               <input type="tel" name="telefono" class="form-control" value="<?php echo ($paciente['telefono']!=null)? $paciente['telefono']:''; ?>">
            </div>
          </div>
          <div class="row form-group text-left">
            <div class="form-group col-lg-6 col-sm-12">
              <label>Nacionalidad</label>
              <select name="nacionalidad" id="" class="form-control select2" <?php echo ($paciente['id_nacionalidad']!=null)? 'disabled':''; ?> style="height:100% !important;" required>
                <option value="" selected disabled>Seleccione...</option>
                <?php while($p = $paises->fetch_assoc()){ ?>
                  <option value="<?php echo $p['id_pais']?>" <?php echo isset($paciente)? (($paciente['id_nacionalidad']==$p['id_pais'])? 'selected':''):'' ?>><?php echo $p['nombre']?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col-lg-6 col-sm-12">
              <label>Fecha de Nacimiento</label>
              <input type="date" name="fecha_nacimiento" class="form-control" value="<?php echo ($paciente['fecha_nacimiento']!=null)? $paciente['fecha_nacimiento']:''; ?>" required>
            </div>
            <div class="form-group col-lg-6 col-sm-12">
              <label>Localidad</label>
              <select name="localidad" class="form-control select2" style="height:100% !important;">
                <option value="" <?php echo ($paciente['id_localidad']!=null)? '':'selected disabled' ?> >Seleccione...</option>
                <?php while($l = $localidades->fetch_assoc()){ ?>
                  <option value="<?php echo $l['id_localidad'];?>" <?php echo isset($paciente)? (($paciente['id_localidad']==$l['id_localidad'])? 'selected':''):'';?> ><?php echo "{$l['nombre']} - {$l['provincia']}";?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col-lg-6 col-sm-12">
              <label>Dirección</label>
              <input type="text" name="direccion" class="form-control" value="<?php echo ($paciente['direccion']!=null)? $paciente['direccion']:''; ?>" required>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-3">
              <label>Obra Social</label>
              <select name="obra" id="" class="form-control select2" style="height:100% !important;">
              <option value="" selected disabled>Seleccione...</option>
                <?php while($o = $obras->fetch_assoc()){ ?>
                  <option value="<?php echo $o['id_obra_social']?>" <?php echo isset($plan)? (($plan['id_obra_social']===$o['id_obra_social'])? 'selected':''):'' ?>><?php echo $o['nombre']?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-lg-4">
              <label>Plan</label>
              <select name="plan" id="" class="form-control select2" style="height:100% !important;">
                  <?php 
                    if(isset($planes) && $planes->num_rows>0){ 
                      while($p = $planes->fetch_assoc()){
                  ?>
                    <option value="<?php echo $p['id_plan']; ?>" <?php echo ($p['id_plan']===$paciente['id_plan'])? 'selected':''; ?>><?php echo $p['descripcion'];?></option>
                  <?php }} ?>
              </select>
            </div>
            <div class="col-lg-5">
              <label>Número Socio (Obra Social)</label>
              <input type="text" name="socio_obra_social" value="<?php echo isset($paciente['socio_obra_social'])? $paciente['socio_obra_social']:''; ?>" class="form-control">
            </div>
          </div>
        <?php } ?>
        <br>
        <div class="form-group text-center">
          <input type="submit" value="Guardar" class="btn btn-primary btn-block">
        </div>
      </form>
    </div>      
  </div>
</section>

<?php include "../layouts/home_footer.php" ?>
<script>
  let frm = null;

  $('select[name="obra"]').change(e=>{
    planes(e.target.value);
  });

  function planes(obra){
    let select = document.querySelector('select[name="plan"]');
    frm = new FormData();
    frm.append('operacion','lista_planes_os');
    frm.append('obra',obra);
    fetch('/ajaxs/generales',{method:'POST',body:frm})
    .then(r => r.json())
    .then(r =>{
      let option = `<option value="" selected disabled >Seleccione...</option>`;
      r.planes.forEach(p=>{
        option+=`
          <option value="${p.id_plan}">${p.descripcion}</option>
        `;
      });
      select.innerHTML = option;
    });

  }

  document.querySelector("input[name='password_confirm']").addEventListener('keyup',(e)=>{
    if(e.target.value === document.querySelector("input[name='password']").value){
      document.querySelector("input[type='submit']").disabled=false;
      document.getElementById("repeatPass").removeChild(document.querySelector(".text-red"));
    }else{
      document.querySelector("input[type='submit']").disabled=true;
      if(!document.querySelector(".text-red"))
      {
        let spn = document.createElement('span');
        spn.classList = "text-red";
        spn.innerText = " (Las contraseñas no coinciden)";
        document.getElementById("repeatPass").appendChild(spn);
      }
    }
  });
</script>