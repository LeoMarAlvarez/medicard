<?php
  if(isset($_POST['action']))
  {
    if($_POST['action']==='ok'){
      $user = [
        'usuario'  => $_POST['username'],
        'pass'      => $_POST['pass']
      ];
      $rpta = login($user);
    }
  }
?>

<?php include "../layouts/page_head.php" ?>

  <div class="text-center justify-content-center align-items-center d-flex">
    <div class="card m-4" style="width:500px;">
      <div class="card-header text-center">
      	<h3><img width="7.5%" src="/images/logo.png"> Bienvenid@ a Medicard</h3>
      </div>
      <div class="card">
        <div class="card-body login-card-body">
          <p class="login-box-msg">Ingrese con su usuario</p>
          <form action="" method="post" autocomplete="off">    
            <input type="hidden" name="action" value="ok">
            <div class="input-group mb-3">
              <input type="text" name="username" class="form-control" placeholder="Usuario">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fa fa-user-circle-o"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" name="pass" class="form-control" placeholder="Contraseña">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fa fa-lock"></span>
                </div>
              </div>
            </div>
            <?php  if(isset($rpta) && $rpta!=''){ ?>
              <div class="alert <?php echo $rpta['alert-type'];?>">
                <?php echo $rpta['msg']; ?>
              </div>
            <?php } ?>
            <div class="row">
              <div class="col-12">
                <button type="submit" value="Login" class="btn btn-primary btn-block">Iniciar Sesión</button>
              </div>
            </div>
          </form>
          <p class="mb-1">
            <a href="/restablecer" class="nav-link text-right">Olvidé mi contraseña</a>
          </p>
          <p class="mb-0">
            <a href="/registro" class="nav-link text-right">Registrarse</a>
          </p>
        </div>  
      </div>
    </div>
  </div>

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>

<script>
  document.addEventListener('DOMContentLoaded',()=>{
    let alerta = document.querySelector('.alert');
    if(alerta != null){
      setTimeout(() => {
        alerta.remove();
      }, 3000);
    }
  });
</script>

<?php include "../layouts/page_foot.php" ?>
