<?php
function checkLogin() {
  session_start();
  $r = ['rol'=>0, 'class'=>''];
  if(isset($_SESSION['user']))
  {
    $r['rol'] = $_SESSION['user']['rol'];
    switch($_SESSION['user']['rol']){
      case 1:
        $r['class'] = 'skin-blue';
        $r['inicio'] = "/administrador/inicio";
        break;
      case 2:
        $r['class'] = 'skin-red';
        $r['inicio'] = "/medico/inicio";
        break;
      case 3:
        $r['class'] = 'skin-purple';
        $r['inicio'] = "/secretaria/inicio";
        break;
      case 4:
        $r['class'] = 'skin-green';
        $r['inicio'] = "/paciente/inicio";
        break;
    }
  }
  return $r;
}

function login($usuario){
  global $mysqli;
  $consulta = $mysqli->query("SELECT * from usuarios where usuario='{$usuario['usuario']}' LIMIT 1");
  if($consulta->num_rows > 0)
  {
    $user = $consulta->fetch_assoc();
    if($user['habilitado']){
      if($user['password'] === $usuario['pass'])
      {
        session_start();
        $_SESSION['user'] = $user;
        switch($_SESSION['user']['rol']){
          case 1:
            echo "<script>window.location.href='administrador/inicio'</script>";
            break;
          case 2:
            echo "<script>window.location.href='medico/inicio'</script>";
            break;
          case 3:
            echo "<script>window.location.href='secretaria/inicio'</script>";
            break;
          case 4:
            echo "<script>window.location.href='paciente/inicio'</script>";
            $parti = $mysqli->query("SELECT id_paciente from pacientes where id_usuario={$_SESSION['user']['id_usuario']}");
            $_SESSION['paciente']=$parti->fetch_assoc();
            $_SESSION['recien_ingresa']=1;
            break;
        }
        // echo "<script>window.location.href='home'</script>";
      }else{
        return [
          'alert-type'=>'alert alert-warning',
          'msg'=>'La contraseña es incorrecta'
        ];
      }
    }else{
      return [
        'alert-type'=>'alert alert-warning',
        'msg'=>'Ud se encuentra inhabilitado para ingresar al sistema'
      ];
    }
  }else{
    return [
        'alert-type'=>"alert alert-danger",
        'msg'=>"{$usuario['usuario']} no está registrado cómo usuario"
      ];
  }
}

function logout(){
  session_start();
  unset($_SESSION);
  session_unset();
  session_destroy();
  echo "<script>window.location.href='/login'</script>";
}

function permisos($r){
  global $url;
  switch($r){
    case 1:
      if($url[0] != 'administrador' && $url[0] != 'mi-perfil')
      {
        echo "<script>alert('Ud no tiene permisos para acceder al sigte enlace'); window.location.href='/administrador/inicio';</script>";
      }
      break;
    case 2:
      if($url[0] != 'medico' && $url[0] != 'mi-perfil')
      {
        echo "<script>alert('Ud no tiene permisos para acceder al sigte enlace'); window.location.href='/medico/inicio';</script>";
      }
      break;
    case 3:
      if($url[0] != 'secretaria' && $url[0] != 'mi-perfil')
      {
        echo "<script>alert('Ud no tiene permisos para acceder al sigte enlace'); window.location.href='/secretaria/inicio';</script>";
      }
      break;
    case 4:
      if($url[0] != 'paciente' && $url[0] != 'mi-perfil')
      {
        echo "<script>alert('Ud no tiene permisos para acceder al sigte enlace'); window.location.href='/paciente/inicio';</script>";
      }
      break;
  }
}


?>