<?php 
  include "../layouts/home_header.php" ;

  $ahora = new DateTime();
  
  $turnos = $mysqli->query("select t.id_turno, t.hora_propuesta, t.hora_fin, e.descripcion as 'e_desc', pac.nombre 'nombre', pac.apellido 'apellido', pac.dni, t.id_estado, 
	  if(p.id_plan is null, 'no tiene', (select po.descripcion from planes_os po where po.id_plan=p.id_plan)) as plan, 
    if(p.id_plan is null, 'no tiene', (select os.nombre from obras_sociales os join planes_os po2 on os.id_obra_social=po2.id_obra_social where po2.id_plan=p.id_plan)) as obra
	  from turnos t join usuarios pac join pacientes p join estados_turno e
    on t.id_paciente=p.id_paciente and p.id_usuario = pac.id_usuario and t.id_estado=e.id_estado
    where t.fecha='{$ahora->format('Y-m-d')}' and t.id_doctor={$_SESSION['user']['id_usuario']} and (t.id_estado=5 or t.id_estado=3) order by t.hora_propuesta, pac.apellido, pac.nombre asc ");

  $turnoActual = $mysqli->query("SELECT * from turnos where id_estado=3 and hora_fin is null and fecha='{$ahora->format('Y-m-d')}' and id_doctor={$_SESSION['user']['id_usuario']} limit 1");
  if($turnoActual->num_rows>0)
  {
    $turnoActual = $turnoActual->fetch_assoc();
  }else{
    $turnoActual=null;
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Turnos del día
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="box box-default">
    <div class="box-header">
      <h1>Presentes</h1>
    </div>
    <div class="box-body">
      <?php if($turnos->num_rows > 0){ ?>
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Hora</th>
              <th>Apellido y Nombre</th>
              <th>DNI</th>
              <th>Obra Social</th>
              <th>Estado</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php while($t = $turnos->fetch_assoc()){ ?>
              <tr class="<?php echo ($turnoActual!=null)? (($turnoActual['id_turno']===$t['id_turno'])? "label-success":""):""; ?>" >
                <td><?php echo ($turnoActual!=null)? (($t['id_turno']===$turnoActual['id_turno'])? "(En curso)":$t['hora_propuesta']):$t['hora_propuesta'] ?></td>
                <td><?php echo "{$t['apellido']} {$t['nombre']}"; ?></td>
                <td><?php echo "{$t['dni']}"; ?></td>
                <td><?php echo "{$t['obra']} ({$t['plan']})"; ?></td>
                <td><?php echo ($t['id_estado']==5)? "<span class='label label-primary'>{$t['e_desc']}</span>":"<span class='label label-default'>{$t['e_desc']}</span>" ?></td>
                <td>
                  <?php if($turnoActual!=null){ ?>
                    <a href="/medico/turno-actual/<?php echo $t['id_turno']; ?>" class="btn btn-xs <?php echo ($turnoActual['id_turno']===$t['id_turno'])? " btn-info ":' btn-default disabled' ?>" title="<?php echo ($turnoActual['id_turno']===$t['id_turno'])? "Turno en curso":"Atender";?>"><i class="fa fa-hand-paper-o"></i></a>    
                  <?php }else if($t['hora_fin']==null){ ?>
                    <a href="/medico/turno-actual/<?php echo $t['id_turno']; ?>" class="btn btn-xs btn-info"><i class="fa fa-hand-paper-o"></i></a>
                  <?php } ?>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php }else{ ?>
        <div class="badge badge-danger">Ningún presente</div>
      <?php } ?>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>
<script>
  $(document).ready(()=>{
    $('.table').DataTable({
        'language':lang
      });
  });
</script>