<?php 
  include "../layouts/home_header.php" ;
  $ahora = new DateTime();
  $turno= $mysqli->query("select * from turnos where id_turno={$url[2]}");
  $turno= $turno->fetch_assoc();
  $paciente= $mysqli->query("select u.nombre, u.apellido, u.dni, p.id_paciente, 
            if(p.id_plan is null, 'no tiene', (select po.descripcion from planes_os po where po.id_plan=p.id_plan)) as plan,
            (if(p.id_plan is null, 'no tiene', (select os.nombre from obras_sociales os join planes_os po2 on os.id_obra_social=po2.id_obra_social where po2.id_plan=p.id_plan))) as obra
            from usuarios u join pacientes p
            on u.id_usuario = p.id_usuario
            where p.id_paciente={$turno['id_paciente']}");
  $paciente = $paciente->fetch_assoc();
  $historial = $mysqli->query("select t.id_turno, t.observacion, t.fecha, u.nombre, u.apellido ".
            "from turnos t join usuarios u on t.id_doctor=u.id_usuario where t.id_paciente={$paciente['id_paciente']} ".
            "and t.fecha <= '".date('Y-m-d')."' and t.id_turno!={$url[2]} and t.id_estado=3 and t.id_especialidad={$turno['id_especialidad']} order by t.fecha desc");
  $antecedentes= $mysqli->query("select * ".
            "from antecedentes as a ".
            "where a.fecha <= '".date('Y-m-d')."' and a.id_paciente={$turno['id_paciente']} ".
            "order by a.fecha desc"
          );
  $cantAntc = $antecedentes->num_rows;
  $antecedente= $antecedentes->fetch_assoc();
  //le doy presente cuando ingresa por primera vez
  if($turno['id_estado']==5)
  {
    $mysqli->query("UPDATE turnos set hora_inicio='{$ahora->format('H:i:s')}', id_estado=3 where id_turno={$url[2]}");
  }
  //actualizo los datos al finalizar el turno
  if(isset($_POST['action']))
  {
    $obs = ($_POST['observaciones']!='')? $_POST['observaciones']:'Sin comentarios ';
    //actualizar antecedente
    if($antecedente!=null){
      if( 
        ($antecedente['peso'] != $_POST['peso']) || ($antecedente['altura'] != $_POST['altura']) || ($antecedente['temperatura'] != $_POST['temperatura']) || ($antecedente['glucemia'] != $_POST['glucemia']) || ($antecedente['presion_arterial_lo'] != $_POST['presion_arterial_lo']) || ($antecedente['presion_arterial_hi'] != $_POST['presion_arterial_hi']) 
      )
      {
        $obs = $obs."
        
        Antecedente
        
        Peso: {$_POST['peso']} |Kg
        Altura: {$_POST['altura']} |m
        Temperatura: {$_POST['temperatura']} |°C
        Glucemia: {$_POST['glucemia']} |mg/dL
        Presión Aterial: {$_POST['presion_arterial_lo']} |Baja
        Presión Aterial: {$_POST['presion_arterial_hi']} |Alta
        ";
        $mysqli->query("INSERT into antecedentes (id_paciente, fecha, hora, peso, altura, temperatura, glucemia, 
          presion_arterial_hi, presion_arterial_lo) values('{$turno['id_paciente']}','{$ahora->format('Y-m-d')}','{$ahora->format('H:i:s')}',
          '{$_POST['peso']}','{$_POST['altura']}','{$_POST['temperatura']}','{$_POST['glucemia']}',
          '{$_POST['presion_arterial_hi']}','{$_POST['presion_arterial_lo']}')"); 

      }
    }else{
      if($_POST['peso']!=null ||$_POST['altura']!=null ||$_POST['temperatura']!=null ||$_POST['glucemia']!=null ||$_POST['presion_arterial_lo']!=null ||$_POST['presion_arterial_hi']!=null){
        $obs = $obs."
        
        Antecedente
        
        Peso: {$_POST['peso']} |Kg
        Altura: {$_POST['altura']} |m
        Temperatura: {$_POST['temperatura']} |°C
        Glucemia: {$_POST['glucemia']} |mg/dL
        Presión Aterial: {$_POST['presion_arterial_lo']} |Baja
        Presión Aterial: {$_POST['presion_arterial_hi']} |Alta
        ";
        $mysqli->query("INSERT into antecedentes (id_paciente, fecha, hora, peso, altura, temperatura, glucemia, 
          presion_arterial_hi, presion_arterial_lo) values('{$turno['id_paciente']}','{$ahora->format('Y-m-d')}','{$ahora->format('H:i:s')}',
          '{$_POST['peso']}','{$_POST['altura']}','{$_POST['temperatura']}','{$_POST['glucemia']}',
          '{$_POST['presion_arterial_hi']}','{$_POST['presion_arterial_lo']}')"); 
      }
    }
    $mysqli->query("UPDATE turnos set id_estado=3, hora_fin='{$ahora->format('H:i:s')}', observacion='{$obs}' where id_turno={$url[2]}");
    if($mysqli->errno!=0)
    {
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/medico/inicio');</script>";
    }else{
      echo "<script>showMsg('success','Cambios Guardados!!','/medico/inicio');</script>";
    }
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Turno Actual
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="box box-default">
    <div class="box-body">
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Paciente</th>
              <th>Obra Social</th>
              <th>Plan OS</th>
            </tr>
          </thead>
          <tbody>
              <tr>
                <td><?php echo "{$paciente['apellido']} {$paciente['nombre']}"; ?></td>
                <td><?php echo "{$paciente['obra']}"; ?></td>
                <td><?php echo "({$paciente['plan']})"; ?></td>
              </tr>
          </tbody>
        </table>
    </div>
    <div class="box-header">
      <h4>Ultimos Antecedentes</h4>
    </div>
    <div class="box-body">
      <form action="" method="post" class="text-left" id="formulario">
        <input type="hidden" name="action" value="ok">
        <div class="row form-group">
          <div class="col-4 col-md-2">
            <label>Peso | <small>(Kg)</small></label>
            <input type="number" name="peso" step=".050" min="0" class="form-control" placeholder="Peso" value="<?php echo ($antecedente!=null)? $antecedente['peso']:''; ?>">
          </div>
          <div class="col-4 col-md-2">
            <label>Altura | <small>(m)</small></label>
            <input type="number" name="altura" step=".05" min="1" max="2" class="form-control" placeholder="Altura" min="1" max="2" step=".05" value="<?php echo ($antecedente!=null)? $antecedente['altura']:''; ?>">
          </div>
          <div class="col-4 col-md-2">
            <label>Temperatura | <small>(°C)</small></label>
            <input type="number" name="temperatura" step=".5" min="35" max="41" class="form-control" placeholder="Temperatura" min="30" max="45" value="<?php echo ($antecedente!=null)? $antecedente['temperatura']:''; ?>">
          </div>
          <div class="col-4 col-md-2">
            <label>Glucemia | <small>(mg/dL)</small></label>
            <input type="number" name="glucemia" min="10" max="300" class="form-control" placeholder="Glucemia" min="0" max="500" value="<?php echo ($antecedente!=null)? $antecedente['glucemia']:''; ?>">
          </div>
          <div class="col-4 col-md-2">
            <label>Presión Arterial | <small>(baja)</small></label>
            <input type="number" name="presion_arterial_lo" step="5" min="50" max="100" class="form-control" placeholder="Presion Baja" min="10" max="150" value="<?php echo ($antecedente!=null)? $antecedente['presion_arterial_lo']:''; ?>">
          </div>
           <div class="col-4 col-md-2">
            <label>Presión Arterial | <small>(alta)</small></label>
            <input type="number" name="presion_arterial_hi" step="5" min="90" max="240" class="form-control" placeholder="Presion Alta" min="90" max="250" value="<?php echo ($antecedente!=null)? $antecedente['presion_arterial_hi']:''; ?>">
          </div>
        </div>
        <div class="row form-group">
          <div class="col-12 col-md-12">
            <h5 class="card-title" align="left">Observaciones</h5>
            <textarea type="textarea" name="observaciones" class="form-control" placeholder="Escriba aqui..." ></textarea>
          </div>
        </div>
        <div class="form-group text-center col-12">
          <input type="submit" value="Finalizar Turno" class="btn btn-primary btn-block">
        </div>
      </form>
        <br>
        <div class="row form-group">
          <div class="col-12 col-md-6">
            <h4 class="card-title text-center" align="left">Historial de Turnos</h4>
            <?php if($historial->num_rows > 0){ ?>
              <table class="table table-striped table-hover table-data">
                <thead>
                  <tr>
                    <th>Fecha</th>
                    <th>Doctor</th>
                    <th>Ver mas</th>
                  </tr>
                </thead>
                <tbody>
                  <?php while($t = $historial->fetch_assoc()){ ?>
                    <tr>
                      <td><?php echo dia($t['fecha']); ?></td>
                      <td><?php echo "{$t['apellido']} {$t['nombre']}"; ?></td>
                      <td><button class="btn btn-info btn-xs btn-obs" data-toggle="modal" data-target="#modal-default" data-observation="<?php echo "{$t['observacion']}"; ?>"><i class="fa fa-eye"></i></button></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
          <?php }else{ ?>
            <div class="badge badge-danger">No tiene historial</div>
          <?php } ?>
          </div>
          <div class="col-12 col-md-6">
            <h4 class="card-title text-center" align="left">Historial de Antecedentes</h4>
            <?php if($cantAntc > 1){ ?>
              <table class="table table-striped table-hover table-data">
                <thead>
                  <tr>
                    <th>Fecha</th>
                    <th>Peso</th>
                    <th>Temperatura</th>
                    <th>Glucemia</th>
                    <th>Presión</th>
                  </tr>
                </thead>
                <tbody>
                  <?php while($a = $antecedentes->fetch_assoc()){ ?>
                    <tr>
                      <td><?php echo dia($a['fecha']); ?></td>
                      <td><?php echo "{$a['peso']}"; ?></td>
                      <td><?php echo "{$a['temperatura']}"; ?></td>
                      <td><?php echo "{$a['glucemia']}"; ?></td>
                      <td><?php echo "{$a['presion_arterial_lo']} / {$a['presion_arterial_hi']}"; ?></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            <?php }else{ ?>
              <div class="container"><span class="label label-secondary">No tiene antecedentes</span></div>
            <?php } ?>
          </div>
        </div>
        <br>

      <!-- modal -->
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Observacion</h4>
            </div>
            <div class="modal-body">
              <p id="obs"></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script>
  $('.btn-obs').click(function (e){
    e.preventDefault();
    $('#obs').text($(this).data('observation'));
  });
  $('.table-data').DataTable({
    'language':lang,
    "order": [ 0, "desc" ]
  });
</script>