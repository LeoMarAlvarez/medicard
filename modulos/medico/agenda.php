<?php 
  include "../layouts/home_header.php" ;
  // $turnos = $mysqli->query("select t.id_turno, u.apellido, u.nombre, u.dni, t.hora_propuesta, 
	//   if(p.id_plan=!null, (select os.nombre from obras_sociales os join planes_os po2 on os.id_obra_social=po2.id_obra_social where po2.id_plan=p.id_plan), 'no tiene') as obra,
	//   if(p.id_plan=!null, (select po.descripcion from planes_os po where po.id_plan=p.id_plan),'') as plan
	//   from turnos t join pacientes p join usuarios u
	//   on t.id_paciente=p.id_paciente and p.id_usuario=u.id_usuario
  //   where t.fecha>='".date('Y-m-d')."' and t.id_doctor={$_SESSION['user']['id_usuario']} and t.id_estado=1 
  //   order by t.fecha, t.hora_propuesta asc");
?>
<link rel="stylesheet" href="/plugins/fullcalendar/main.min.css">

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Mi Agenda
  </h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-body">
      <div id="calendar"></div>
    </div>
  </div>
</section>

<?php include "../layouts/home_footer.php" ?>
<script src="/plugins/fullcalendar/main.min.js"></script>
<script src="/plugins/fullcalendar/locales-all.min.js"></script>
<script src="/js/agenda.js"></script>
