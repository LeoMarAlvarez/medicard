<?php 
  include "../layouts/home_header.php" ;
  $horario=null;
  $especialidades = $mysqli->query("SELECT * from especialidades e where e.id_especialidad in (SELECT epd.id_especialidad from especialidades_por_doctor epd where epd.habilitado=1 and epd.id_doctor={$_SESSION['user']['id_usuario']})");
  $dias = $mysqli->query("SELECT * from dias order by id_dia");
  if(isset($url[2]))
  {
    $r = $mysqli->query("SELECT * from horarios where id_hora=".$url[2]." limit 1");
    $horario = $r->fetch_assoc();
  }
  if(isset($_POST['action']))
  {
    $horaIni = new DateTime($_POST['hora_inicio']);
    $horaFin = new DateTime($_POST['hora_fin']);
    $horaIni = $horaIni->modify('+1 second')->format('H:i:s');
    $horaFin = $horaFin->modify('-1 second')->format('H:i:s');
    if($_POST['action']==='mod'){
      $checkHora = $mysqli->query("select * from horarios h where ".
          "((('{$horaIni}' between h.hora_inicio and h.hora_fin) or ('{$horaFin}' between h.hora_inicio and h.hora_fin)) ".
          "or ".
          "((h.hora_inicio between '{$horaIni}' and '{$horaFin}') or (h.hora_fin between '{$horaIni}' and '{$horaFin}'))) ".
          "and h.id_dia={$_POST['dia']} and h.id_hora!={$_POST['id']} and h.id_doctor={$_SESSION['user']['id_usuario']}");
    }else{
      $checkHora = $mysqli->query("select * from horarios h where ".
          "((('{$horaIni}' between h.hora_inicio and h.hora_fin) or ('{$horaFin}' between h.hora_inicio and h.hora_fin)) ".
          "or ".
          "((h.hora_inicio between '{$horaIni}' and '{$horaFin}') or (h.hora_fin between '{$horaIni}' and '{$horaFin}'))) ".
          "and h.id_dia={$_POST['dia']} and h.id_doctor={$_SESSION['user']['id_usuario']}");
    }

    if($checkHora->num_rows>0){
      echo "<script>showMsg('warning','Actualmente tenés un horario en ese día que se interpone con el que estás tratando de cargar.','/medico/mis-horarios');</script>";
    }else{
      if($_POST['action']==='ok')
      {
        $mysqli->query("INSERT into horarios (id_dia, hora_inicio, hora_fin, id_doctor, id_especialidad,minutos_por_turno) values (".$_POST['dia'].",'".$_POST['hora_inicio']."','".$_POST['hora_fin']."',".$_SESSION['user']['id_usuario'].", ".$_POST['especialidad'].", ".$_POST['minutos'].")");
      }else{
        $check = isset($_POST['habilitado'])? 1:0;
        $mysqli->query("UPDATE horarios set id_dia=". $_POST['dia'] . ", hora_inicio='". $_POST['hora_inicio'] . "', hora_fin='".$_POST['hora_fin']."', habilitado=".$check.", id_especialidad=".$_POST['especialidad'].", minutos_por_turno=".$_POST['minutos']." where id_hora=$_POST[id]");
      }

      if($mysqli->errno != 0){
        echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/medico/mis-horarios');</script>";
      }else{
        echo "<script>showMsg('success','Se han guardado los cambios','/medico/mis-horarios');</script>";
      }
    }
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Mis Horarios
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Formulario -->
  <div class="box box-default" id="formulario">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo isset($url[2])?'Editar Horario':'Cargar Nuevo Horario'; ?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <form action="" method="post" class="">
        <input type="hidden" name="action" value="<?php echo ($horario!=null)? 'mod':'ok'; ?>">
        <input type="hidden" name="id" value="<?php echo ($horario!=null)? $horario['id_hora']:''; ?>">
        <div class="form-group col-lg-4 col-md-6 col-sm-12">
          <label>Hora de Inicio</label>
          <input type="time" name="hora_inicio"  required class="form-control" value="<?php echo ($horario!=null)? $horario['hora_inicio']:''; ?>">
        </div>
        <div class="form-group col-lg-4 col-md-6 col-sm-12">
          <label>Hora de Fín</label>
          <input type="time" name="hora_fin" id="" required  class="form-control" value="<?php echo ($horario!=null)? $horario['hora_fin']:''; ?>">
        </div>
        <div class="form-group col-lg-4 col-md-6 col-sm-12">
          <label>Día</label>
          <select name="dia"  required class="form-control">
            <?php while($dia = $dias->fetch_assoc()){ ?>
              <option value="<?php echo $dia['id_dia']?>" <?php echo ($horario!=null)? (($horario['id_dia']==$dia['id_dia'])? 'selected':''):''; ?> ><?php echo $dia['nombre']?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-lg-4 col-md-6 col-sm-12">
          <label>Especialidad</label>
          <select name="especialidad"  required class="form-control">
            <?php while($especialidad = $especialidades->fetch_assoc()){ ?>
              <option value="<?php echo $especialidad['id_especialidad']?>" <?php echo ($horario!=null)? (($horario['id_especialidad']==$especialidad['id_especialidad'])? 'selected':''):''; ?> ><?php echo $especialidad['descripcion']?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-lg-4 col-sm-12">
          <label>Minutos por turno</label>
          <input type="number" name="minutos" min="5" step="5" class="form-control" value="<?php echo ($horario!=null)? $horario['minutos_por_turno']:20?>">
        </div>
        <div class="form-group col-lg-4 col-md-6 col-sm-12">
          <div class="checkbox">
            <label><input type="checkbox" name="habilitado" <?php echo isset($horario)? (($horario['habilitado']==1)? 'checked':''):''; ?> >Habilitado</label>
          </div>
        </div>
        <div class="form-group col-lg-12 text-center">
          <input type="submit" value="Guardar" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-header with-border">
      <h3 class="box-title">Listado de Horarios</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
          <table class="table p-2 m-3 table-striped text-center">
            <thead>
              <tr>
                <th>Día</th>
                <th>Hora Inicio</th>
                <th>Hora Fin</th>
                <th>Especialidad</th>
                <th>Minutos por Turno</th>
                <th>Estado</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody id="tbl_horarios"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script>
  $(document).ready(()=>{
    cargarLista('');
  });
  
  function cargarLista(search){
    const data = new FormData();
    data.append('search',search);
    data.append('operacion','horarios_doctor');
    data.append('doctor',"<?php echo $_SESSION['user']['id_usuario'];?>");
    fetch(
      '/ajaxs/medico', 
      {
        method:'POST',
        body: data
      })
    .then(r => r.json())
    .then(rpta =>{
      let horarios = rpta.horarios;
      let filas="";
      let estado='',clas='';
      horarios.forEach(h =>{
        if(h.habilitado==1)
        {
          estado = "Activo";
          clas = "text-green";
        }else{
          estado = "Inactivo";
          clas = "text-danger";
        }
        filas += `
          <tr>
            <td>${h.nombre}</td>
            <td>${h.hora_inicio}</td>
            <td>${h.hora_fin}</td>
            <td><span class="label label-info">${h.descripcion}</span></td>
            <td>${h.minutos_por_turno}</td>
            <td><span class="${clas}">${estado}</span></td>
            <td>
              <a href="mis-horarios/${h.id_hora}" class="a btn btn-primary btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
              <button class="btn btn-xs btn-danger btnBaja" onclick="eliminar('id_hora',${h.id_hora},'horarios')" title="Inhabilitar"><i class="fa fa-trash"></i></button>
            </td>
          </tr>
        `;
      });
      document.querySelector('#tbl_horarios').innerHTML = filas;
      $('.table').DataTable({
        'language':lang
      });
    });
  }
</script>