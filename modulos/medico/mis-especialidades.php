<?php 
  include "../layouts/home_header.php" ;
  //actualizar sobreturnos
  if(isset($_POST['id']))
  {
    $mysqli->query("UPDATE especialidades_por_doctor set sobreturnos={$_POST['sobreturnos']} where id={$_POST['id']}");
    if($mysqli->errno != 0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/medico/mis-especialidades');</script>";
    }else{
      echo "<script>showMsg('success','Cambios Guardados!!','/medico/mis-especialidades');</script>";
    }
  }
  //habilitar-deshabilitar especialidad
  if(isset($url[2]) && isset($url[3])){
    $mysqli->query("UPDATE especialidades_por_doctor set habilitado=".$url[3]." where id=".$url[2]);
    if($mysqli->errno != 0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/medico/mis-especialidades');</script>";
    }else{
      echo "<script>showMsg('success','Cambios Guardados!!','/medico/mis-especialidades');</script>";
    }
  }
  //Insertar especialidad
  if(isset($_POST['especialidad'])){
    $mysqli->query("INSERT into especialidades_por_doctor (id_especialidad, id_doctor,sobreturnos) values(".$_POST['especialidad'].",".$_SESSION['user']['id_usuario'].",{$_POST['sobreturnos']})");
    if($mysqli->errno != 0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/medico/mis-especialidades');</script>";
    }else{
      echo "<script>showMsg('success','Se han guardado los cambios','/medico/mis-especialidades');</script>";
    }
  }

  $especialidades = $mysqli->query("SELECT * from especialidades e where e.id_especialidad not in (select id_especialidad from especialidades_por_doctor epd where epd.id_doctor={$_SESSION['user']['id_usuario']})");
  $mis_especialidades = $mysqli->query("SELECT ed.id, ed.habilitado, e.descripcion, ed.sobreturnos from especialidades_por_doctor ed join especialidades e on ed.id_especialidad=e.id_especialidad where ed.id_doctor=".$_SESSION['user']['id_usuario']." group by e.id_especialidad");
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Mis Especialidades
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="box box-default">
    <div class="box-header with-border">
      <form action="" method="POST" autocomplete="off" class="text-left">
        <div class="col-lg-6 form-group">
          <label for="">Especialidad</label>
          <select name="especialidad" class="form-control select2" required>
            <option value="" disable>Seleccione...</option>
            <?php while($e = $especialidades->fetch_assoc()){ ?>
              <option value="<?php echo $e['id_especialidad']; ?>"><?php echo $e['descripcion']; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col-lg-3 form-group">
          <label>Sobreturnos | <small>(cantidad)</small></label>
          <input type="number" name="sobreturnos" class="form-control" min="0" value="0" required>
        </div>
        <div class="col-lg-3 form-group">
          <label class=""> </label>
          <button type="submit" class="btn btn-primary form-control">Guardar</button>
        </div>
      </form>
    </div>
    <div class="box-body">
      <?php if($mis_especialidades->num_rows > 0){ ?>
        <h2>Lista de Especialidades</h2><br>
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Especialidad</th>
              <th>Sobreturnos | <small>(cantidad)</small></th>
              <th>Estado</th>
              <th>Operación | <small>(Activar/Desactivar)</small></th>
            </tr>
          </thead>
          <tbody>
            <?php while($e = $mis_especialidades->fetch_assoc()){ ?>
              <tr>
                <td><?php echo "{$e['descripcion']}"; ?></td>
                <td>
                  <form action="" method="post">
                    <input type="hidden" name="id" value="<?php echo $e['id']?>">
                    <input type="number" name="sobreturnos"  min="0" value="<?php echo $e['sobreturnos'];?>" class="form-control inline" style="width:10%; min-width:60px;">
                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i></button>
                  </form>
                </td>
                <td><?php echo ($e['habilitado']!=0)? "<span class='label label-primary'>Activo</span>":"<span class='label label-danger'>Inactivo</span>"; ?></td>
                <td>
                  <?php if($e['habilitado']!=0){?>
                      <a href="mis-especialidades/<?php echo $e['id'].'/0';?>" class="btn btn-danger btn-xs" title="Deshabilitar"><i class="fa fa-eye-slash"></i></a>
                  <?php }else{?>
                      <a href="mis-especialidades/<?php echo $e['id'].'/1';?>" class="btn btn-primary btn-xs" title="Habilitar"><i class="fa fa-eye"></i></a>
                  <?php }?>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php }else{ ?>
        <div class="container badge badge-danger">No tienes Especialidades cargadas</div>
      <?php } ?>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>
<script>
  $(document).ready(()=>{
    $('.table').DataTable({
        'language':lang
      });
  });
</script>