<?php 
  include "../layouts/home_header.php" ;
  // global $url;
  // global $mysqli;
  // permisos();
  $plan=null;
  $obras = $mysqli->query("SELECT * from obras_sociales where habilitado=1 order by nombre asc");
  if(isset($url[2]))
  {
    $r = $mysqli->query("select * from planes_os where id_plan=".$url[2]." limit 1");
    $plan = $r->fetch_assoc();
  }
  if(isset($_POST['action']))
  {
    if($_POST['action']==='ok')
    {
      $mysqli->query("INSERT into planes_os (descripcion,id_obra_social) values ('{$_POST['nombre']}', {$_POST['obra']})");
    }else{
      $checked= isset($_POST['habilitado'])? 1:0;
      $mysqli->query("UPDATE planes_os set descripcion='".$_POST['nombre'] . "', habilitado=". $checked . ", id_obra_social=".$_POST['obra']." where id_plan=".$_POST['id']);
    }
    if($mysqli->errno != 0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/administrador/planes-obras-sociales');</script>";
    }else{
      echo "<script>showMsg('success','Se han guardado los cambios','/administrador/planes-obras-sociales');</script>";
    }
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Panel Administrativo de Planes
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Formulario -->
  <div class="box box-default" id="formulario">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo isset($url[2])?'Editar Plan':'Agregar Plan'; ?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <form action="" method="post" class="" autocomplete="off">
        <input type="hidden" name="action" value="<?php echo ($plan!=null)? 'mod':'ok'; ?>">
        <input type="hidden" name="id" value="<?php echo ($plan!=null)? $plan['id_plan']:''; ?>">
        <div class="form-group row">
          <label class="col-lg-3 text-right">Nombre</label>
          <div class="col-lg-6">
            <input type="text" name="nombre" class="form-control" value="<?php echo ($plan!=null)? $plan['descripcion']:'';?>">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-3 text-right">Obra Social</label>
          <div class="col-lg-6">
            <select name="obra" id="" class="form-control">
              <?php while($o = $obras->fetch_assoc()){ ?>
                <option value="<?php echo $o['id_obra_social'] ?>" <?php echo ($plan!=null)? (($plan['id_obra_social']==$o['id_obra_social'])? 'selected':''):''; ?>><?php echo $o['nombre']?></option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-lg-6">
            <input type="checkbox" id="habilitado" name="habilitado" <?php echo ($plan!=null)? (($plan['habilitado']==1)? 'checked':''):'';?>>
            <label for="habilitado"> Habilitado</label>
          </div>
        </div>
        <div class="form-group text-center">
          <input type="submit" value="Guardar" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-header with-border">
      <h3 class="box-title">Listado de Planes</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
          <table class="table p-2 m-3 table-striped">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Obra Social</th>
                <th>Habilitado</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody id="tbl_planes"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script>
  $(document).ready(()=>{
    cargarLista('');
  });
  
  function cargarLista(search){
    const data = new FormData();
    data.append('search',search);
    data.append('operacion','planes');
    fetch(
      '/ajaxs/admin', 
      {
        method:'POST',
        body: data
      })
    .then(r => r.json())
    .then(rpta =>{
      let planes = rpta.planes;
      let filas="";
      planes.forEach(p =>{
        if(p.habilitado==1)
        {
          estado="Activo";
          clas="text-green";
        }else{
          estado="Inactivo";
          clas="text-danger";
        }
        filas += `
          <tr>
            <td>${p.descripcion}</td>
            <td>${p.nombre}</td>
            <td><span class="${clas}">${estado}</span></td>
            <td>
              <a href="/administrador/planes-obras-sociales/${p.id_plan}" class="a btn btn-primary btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
              <button class="btn btn-xs btn-danger btnBaja" onclick="eliminar('id_plan',${p.id_plan},'planes_os')" title="Inhabilitar"><i class="fa fa-trash"></i></button>
            </td>
          </tr>
        `;
      });
      document.querySelector('#tbl_planes').innerHTML = filas;
      $('.table').DataTable({
        'language':lang
      });
    });
  }
</script>