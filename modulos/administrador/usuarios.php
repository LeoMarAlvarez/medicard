<?php 
  include "../layouts/home_header.php" ;
  // global $url;
  // global $mysqli;
  // permisos();
  $usuario=null;
  $roles=$mysqli->query("SELECT * from roles order by id_rol asc");
  if(isset($url[2]))
  {
    $r = $mysqli->query("SELECT * from usuarios where id_usuario=".$url[2]." limit 1");
    $usuario = $r->fetch_assoc();
  }
  if(isset($_POST['action']))
  {
    $pass = ($_POST['password']!='')? $_POST['password']:$_POST['dni'];
    if($_POST['action']==='ok')
    {
      
      $first = $mysqli->query("SELECT * from usuarios where dni='".$_POST['dni']."' or usuario='".$_POST['usuario']."' limit 1"); 

      if($first->num_rows>0)
      {
        echo "<script>showMsg('warning','Ya tenemos un registro con ese usuario o dni. Por favor Revisa los datos y vuelve a intentar. ','');</script>";
      }else{
        $mysqli->query("INSERT into usuarios (dni, nombre, apellido, usuario, password, rol, sexo) values ('".$_POST['dni']."','".$_POST['nombre']."','".$_POST['apellido']."','".$_POST['usuario']."','".$pass."',".$_POST['rol'].",'".$_POST['sexo']."')");
        if($_POST['rol']==4){
          $id = $mysqli->insert_id;
          $mysqli->query("INSERT into pacientes (id_usuario) values($id)");
        }
        if($mysqli->errno != 0){
          echo "<script>showMsg('error','Ups!! no pudimos guardar el registro','/administrador/usuarios');</script>";
        }else{
          echo "<script>showMsg('success','Se han guardado los cambios','/administrador/usuarios');</script>";
        }
      }
    }else{
      $noRepes = $mysqli->query("SELECT * from usuarios where (usuario='{$_POST['usuario']}' or dni='{$_POST['dni']}') and id_usuario!={$url[2]}");
      if($noRepes->num_rows>0){
        echo "<script>swal({text:'Ya tenemos un registro con ese usuario o dni. Por faver Revisa los datos y vuelve a intentarlo',icon:'warning'});</script>";
      }else{
        $check = (isset($_POST['habilitado']))? 1:0;
        $sql = "UPDATE usuarios set dni='". $_POST['dni'] . "', nombre='". $_POST['nombre'] . "', apellido='". $_POST['apellido'] . "', usuario='". $_POST['usuario'] . "' , password='". $pass . "' , rol=". $_POST['rol'] . " , habilitado=". $check . " , sexo='". $_POST['sexo'] . "' where id_usuario=".$_POST['id'];
        $mysqli->query($sql);
        if($mysqli->errno != 0){
          echo "<script>showMsg('error','Ups!! no pudimos guardar el registro','/administrador/usuarios');</script>";
        }else{
          echo "<script>showMsg('success','Se han guardado los cambios','/administrador/usuarios');</script>";
        }
      }
    }
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Panel Administrativo de Usuarios
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Formulario -->
  <div class="box box-default" id="formulario">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo isset($url[2])?'Editar Usuario':'Agregar Usuario'; ?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <form action="" method="post" class="text-center" autocomplete="off">
        <input type="hidden" name="action" value="<?php echo ($usuario!=null)? 'mod':'ok'; ?>">
        <input type="hidden" name="id" value="<?php echo ($usuario!=null)? $usuario['id_usuario']:''; ?>">
        <div class="row form-group">
          <label class="col-12 col-md-2 text-right">DNI</label>
          <div class="col-12 col-md-4">
            <input type="text" name="dni" onkeyup="pressInput()" onkeypress="prevPressInput()" class="form-control" minlength="8" value="<?php echo ($usuario!=null)? $usuario['dni']:'';?>" required>
          </div>
          <label class="col-12 col-md-2 text-right">Sexo</label>
          <div class="col-12 col-md-4">
            <select name="sexo" class="form-control">
              <option value="h" <?php echo ($usuario!=null)? (($usuario['sexo']==='h')? 'selected':''):''; ?> >Hombre</option>
              <option value="m" <?php echo ($usuario!=null)? (($usuario['sexo']==='m')? 'selected':''):''; ?> >Mujer</option>
            </select>
          </div>
        </div>
        <div class="row form-group">
          <label class="col-12 col-md-2 text-right">Nombre</label>
          <div class="col-12 col-md-4">
            <input type="text" name="nombre" class="form-control" value="<?php echo ($usuario!=null)? $usuario['nombre']:'';?>" required>
          </div>
          <label class="col-12 col-md-2 text-right">Apellido</label>
          <div class="col-12 col-md-4">
            <input type="text" name="apellido"  class="form-control" value="<?php echo ($usuario!=null)? $usuario['apellido']:'';?>" required>
          </div>
        </div>
        <div class="row form-group">
          <label class="col-12 col-md-2 text-right">Usuario</label>
          <div class="col-12 col-md-4">
            <input type="text" name="usuario" required class="form-control" value="<?php echo ($usuario!=null)? $usuario['usuario']:'';?>" >
          </div>
          <label class="col-12 col-md-2 text-right">Contraseña</label>
          <div class="col-12 col-md-4">
            <input type="password" name="password" placeholder="Por defecto DNI" class="form-control" value="<?php echo ($usuario!=null)? $usuario['password']:'';?>">
          </div>
        </div>
        <div class="row form-group">
          <label class="col-12 col-md-2 text-right">Rol</label>
          <div class="col-12 col-md-4">
            <select name="rol" class="form-control" required>
              <?php while ($r=$roles->fetch_assoc()){?> 
                <option value="<?php echo $r['id_rol'];?>" <?php echo ($usuario!=null)? (($usuario['rol']==$r['id_rol'])? 'selected':''):''; ?>><?php echo $r['nombre'];?></option>
              <?php 
              }?>
            </select>
          </div>
          <div class="col-12 col-md-6">
            <input type="checkbox" id="habilitado" name="habilitado" <?php echo ($usuario!=null)? (($usuario['habilitado']==1)? 'checked':''):'';?>>
            <label for="habilitado"> Habilitado</label>
          </div>
        </div>
        <div class="form-group text-center">
          <input type="submit" value="Guardar" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-header with-border">
      <h3 class="box-title">Listado de Usuarios</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
          <table class="table p-2 m-3 table-striped">
            <thead>
              <tr>
                <th>Apellido</th>
                <th>Nombre</th>
                <th>DNI</th>
                <th>Usuario</th>
                <th>Rol</th>
                <th>Estado</th>
                <th>Operación</th>
              </tr>
            </thead>
            <tbody id="tbl_usuarios"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script>
  $(document).ready(()=>{
    cargarLista('');
  });
  
  function cargarLista(search){
    const data = new FormData();
    data.append('search',search);
    data.append('operacion','usuarios');
    fetch(
      '/ajaxs/admin', 
      {
        method:'POST',
        body: data
      })
    .then(r => r.json())
    .then(rpta =>{
      let usuarios = rpta.usuarios;
      let filas="";
      let estado='',clas='';
      usuarios.forEach(p =>{
        if(p.habilitado==1)
        {
          estado = "Activo";
          clas = "text-green";
        }else{
          estado = "Inactivo";
          clas = "text-danger";
        }
        filas += `
          <tr>
            <td>${p.apellido}</td>
            <td>${p.nombre}</td>
            <td>${p.dni}</td>
            <td>${p.usuario}</td>
            <td>${p.rol}</td>
            <td><span class="${clas}">${estado}</span></td>
            <td>
              <a href="usuarios/${p.id_usuario}" class="a btn btn-primary btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
              <button class="btn btn-xs btn-danger btnBaja" onclick="eliminar('id_usuario',${p.id_usuario},'usuarios')" title="Inhabilitar"><i class="fa fa-trash"></i></button>
            </td>
          </tr>
        `;
      });
      document.querySelector('#tbl_usuarios').innerHTML = filas;
      $('.table').DataTable({
        'language':lang
      });
    });
  }
</script>
<script src="/js/noLetters.js"></script>
