<?php 
  include "../layouts/home_header.php" ;
  $solicitudes = $mysqli->query("SELECT u.apellido, u.nombre, u.dni, s.fecha_solicitud, r.nombre as rol, s.detalle, s.id_solicitud from solicitudes s join usuarios u join roles r on s.id_usuario=u.id_usuario and u.rol=r.id_rol where s.estado=1 order by s.fecha_solicitud asc");
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Panel Principal
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="box box-default">
    <div class="box-header">
      <h3>Solicitudes</h3>
    </div>
    <div class="box-body">
      <?php if($solicitudes->num_rows>0) { ?>
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Fecha de Solicitud</th>
              <th>Apellido y Nombre</th>
              <th>DNI</th>
              <th>Tipo</th>
              <th>Detalle</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php while($s = $solicitudes->fetch_assoc()){ 
                $dia = new DateTime($s['fecha_solicitud']);
              ?>
              <tr>
                <td><?php echo "{$dia->format('d-M-Y')} {$dia->format('H:i')}"; ?></td>
                <td><?php echo "{$s['apellido']} {$s['nombre']}"; ?></td>
                <td><?php echo "{$s['dni']}"; ?></td>
                <td><?php echo "{$s['rol']}"; ?></td>
                <td><?php echo "{$s['detalle']}"; ?></td>
                <td><button class="btn btn-primary btn-md" title="Solucionar" onclick="change(<?php echo $s['id_solicitud']; ?>)"><i class="fa fa-cogs"></i></button></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php } else { ?>
        <div class="badge badge-danger">
          No hay solicitudes para resolver
        </div>
      <?php } ?>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script>
  let form = null;
  
  $(document).ready(()=>{
    $('.table').DataTable({
        'language':lang
      });
  });

  function change(id)
  {
    form = new FormData();
    form.append('operacion', 'solucionar_solicitud');
    form.append('solicitud', Number(id));
    console.log(id);
    fetch('/ajaxs/admin',
    {
      method: 'POST',
      body: form
    })
    .then(r => r.json())
    .then(r => {
      console.log(r);
      if(r.ok){
        swal({text:'Guardado',icon:'success',timer:2000,buttons:false})
        .then(()=>{
          window.location.reload();
        });
      }
    });
  }
</script>