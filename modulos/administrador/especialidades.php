<?php 
  include "../layouts/home_header.php" ;
  $especialidad=null;
  if(isset($url[2]))
  {
    $r = $mysqli->query("select * from especialidades where id_especialidad=".$url[2]." limit 1");
    $especialidad = $r->fetch_assoc();
  }
  if(isset($_POST['action']))
  {
    $habilitado=(isset($_POST['habilitado']))? 1:0;
    if($_POST['action']==='ok')
    {
      $mysqli->query("insert into especialidades (descripcion) values ('". $_POST['descripcion'] ."')");
    }else{      
      $mysqli->query("update especialidades set descripcion='". $_POST['descripcion'] . "', habilitado=". $habilitado . " where id_especialidad=".$_POST['id']);
    }
    if($mysqli->errno != 0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/administrador/especialidades');</script>";
    }else{
      echo "<script>showMsg('success','Se han guardado los cambios','/administrador/especialidades');</script>";
    }
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Panel Administrativo de Especialidades
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Formulario -->
  <div class="box box-default" id="formulario">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo isset($url[2])?'Editar Especialidad':'Agregar Especialidad'; ?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <form action="" method="post" class="container" autocomplete="off">
        <input type="hidden" name="action" value="<?php echo ($especialidad!=null)? 'mod':'ok'; ?>">
        <input type="hidden" name="id" value="<?php echo ($especialidad!=null)? $especialidad['id_especialidad']:''; ?>">
        <div class="form-group row">
          <label class="col-12 col-md-2 text-right">Nombre</label>
          <div class="col-12 col-md-4">
            <input type="text" name="descripcion" class="form-control" value="<?php echo ($especialidad!=null)? $especialidad['descripcion']:'';?>" required>
          </div>
          <div class="col-12 col-md-6" style="">
            <input type="checkbox" id="habilitado" name="habilitado" <?php echo ($especialidad!=null)? (($especialidad['habilitado']==1)? 'checked':''):'';?>>
              <label for="habilitado"> Habilitado</label>              
          </div>
        </div>
        <div class="form-group text-center">
          <input type="submit" value="Guardar" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-header with-border">
      <h3 class="box-title">Listado de Especialidades</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
          <table class="table p-2 m-3 table-striped">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Estado</th>
                <th></th>
              </tr>
            </thead>
            <tbody id="tbl_especialidades"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script>
  $(document).ready(()=>{
    cargarLista('');
  });
  
  function cargarLista(search){
    const data = new FormData();
    data.append('search',search);
    data.append('operacion','especialidades');
    fetch(
      '/ajaxs/admin', 
      {
        method:'POST',
        body: data
      })
    .then(r => r.json())
    .then(rpta =>{
      let especialidades = rpta.especialidades;
      let filas="";
      let estado='', clas='';
      especialidades.forEach(p =>{
        if(p.habilitado==1)
        {
          estado="Activo";
          clas="text-green";
        }else{
          estado="Inactivo";
          clas="text-danger";
        }
        filas += `
          <tr>
            <td>${p.descripcion}</td>
            <td><span class="${clas}">${estado}</span></td>
            <td>
              <a href="especialidades/${p.id_especialidad}" class="a btn btn-primary btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
              <button class="btn btn-xs btn-danger btnBaja" onclick="eliminar('id_especialidad',${p.id_especialidad},'especialidades')" title="Inhabilitar"><i class="fa fa-trash"></i></button>
            </td>
          </tr>
        `;
      });
      document.querySelector('#tbl_especialidades').innerHTML = filas;
      $('.table').DataTable({
        'language':lang
      });
    });
  }
</script>