<?php 
  include "../layouts/home_header.php" ;
  // global $url;
  // global $mysqli;
  // permisos();
  $pais=null;
  if(isset($url[2]))
  {
    $r = $mysqli->query("select * from paises where id_pais=".$url[2]." limit 1");
    $pais = $r->fetch_assoc();
  }
  if(isset($_POST['action']))
  {
    if($_POST['action']==='ok')
    {
      $mysqli->query("insert into paises (sigla, nombre) values ('$_POST[sigla]','$_POST[nombre]')");
    }else{
      $mysqli->query("update paises set sigla='". strtoupper($_POST['sigla']) . "', nombre='". strtoupper($_POST['nombre']) . "' where id_pais=$_POST[id]");
    }
    if($mysqli->errno != 0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/administrador/paises');</script>";
    }else{
      echo "<script>showMsg('success','Se han guardado los cambios','/administrador/paises');</script>";
    }
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Panel Administrativo de Países
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Formulario -->
  <div class="box box-default" id="formulario">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo isset($url[2])?'Editar País':'Agregar País'; ?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <form action="" method="post" class="text-center" autocomplete="off">
        <input type="hidden" name="action" value="<?php echo ($pais!=null)? 'mod':'ok'; ?>">
        <input type="hidden" name="id" value="<?php echo ($pais!=null)? $pais['id_pais']:''; ?>">
        <div class="form-group row">
          <label class="col-12 col-md-2 text-right">Siglas</label>
          <div class="col-12 col-md-4">
            <input type="text" name="sigla" class="form-control" maxlength="2" onkeyup="this.value = this.value.toUpperCase();" required value="<?php echo ($pais!=null)? $pais['sigla']:'';?>">
          </div>
          <label class="col-12 col-md-2 text-right">Nombre</label>
          <div class="col-12 col-md-4">
            <input type="text" name="nombre" class="form-control" required value="<?php echo ($pais!=null)? $pais['nombre']:'';?>">
          </div>
        </div>
        <div class="form-group text-center">
          <input type="submit" value="Guardar" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-header with-border">
      <h3 class="box-title">Listado de Países</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
          <table class="table p-2 m-3 table-striped">
            <thead>
              <tr>
                <th>Sigla</th>
                <th>País</th>
                <th></th>
              </tr>
            </thead>
            <tbody id="tbl_paises"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script>
  $(document).ready(()=>{
    cargarLista('');
  });
  
  function cargarLista(search){
    const data = new FormData();
    data.append('search',search);
    data.append('operacion','paises');
    fetch(
      '/ajaxs/lugares', 
      {
        method:'POST',
        body: data
      })
    .then(r => r.json())
    .then(rpta =>{
      let paises = rpta.paises;
      let filas="";
      paises.forEach(p =>{
        filas += `
          <tr>
            <td>${p.sigla}</td>
            <td>${p.nombre}</td>
            <td>
              <a href="paises/${p.id_pais}" class="a btn btn-primary btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
            </td>
          </tr>
        `;
      });
      document.querySelector('#tbl_paises').innerHTML = filas;
      $('.table').DataTable({
        'language':lang
      });
    });
  }
</script>