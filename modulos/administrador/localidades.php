<?php 
  include "../layouts/home_header.php" ;
  // global $url;
  // global $mysqli;
  // permisos();
  $localidad=null;
  $provincias= $mysqli->query("SELECT * from provincias order by nombre");
  if(isset($url[2]))
  {
    $r = $mysqli->query("SELECT * from localidades where id_localidad=".$url[2]." limit 1");
    $localidad = $r->fetch_assoc();
  }
  if(isset($_POST['action']))
  {
    if($_POST['action']==='ok')
    {
      $mysqli->query("INSERT into localidades (id_provincia, nombre) values ($_POST[provincia],'$_POST[nombre]')");
    }else{
      $mysqli->query("UPDATE localidades set id_provincia=". $_POST['provincia'] . ", nombre='". $_POST['nombre'] . "' where id_localidad=$_POST[id]");
    }
    if($mysqli->errno != 0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/administrador/localidades');</script>";
    }else{
      echo "<script>showMsg('success','Se han guardado los cambios','/administrador/localidades');</script>";
    }
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Panel Administrativo de Localidades
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Formulario -->
  <div class="box box-default" id="formulario">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo isset($url[2])?'Editar Localidad':'Agregar Localidad'; ?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <form action="" method="post" class="text-center" autocomplete="off">
        <input type="hidden" name="action" value="<?php echo ($localidad!=null)? 'mod':'ok'; ?>">
        <input type="hidden" name="id" value="<?php echo ($localidad!=null)? $localidad['id_localidad']:''; ?>">
        <div class="row form-group">
          <label class="col-12 col-md-2 text-right">Nombre Localidad</label>
          <div class="col-12 col-md-4">
            <input type="text" name="nombre" class="form-control" required value="<?php echo ($localidad!=null)? $localidad['nombre']:'';?>">
          </div>
          <label class="col-12 col-md-2 text-right">Provincia</label>
          <div class="col-12 col-md-4">
            <select name="provincia" required class="form-control">
            <?php while($provincia = $provincias->fetch_assoc()){ ?>
                <option value="<?php echo $provincia['id_provincia']?>" <?php echo ($localidad!=null)? (($localidad['id_provincia']==$provincia['id_provincia'])? 'selected':''):''; ?> ><?php echo $provincia['nombre']?></option>
            <?php } ?>
            </select>
          </div> 
        </div>
        <div class="form-group text-center">
          <input type="submit" value="Guardar" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-header with-border">
      <h3 class="box-title">Listado de Localidades</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="form-inline dt-bootstrap">
        <div class="row">
          <div class="col-lg-12">
            <table class="table p-2 m-3 table-striped table-bordered table-hover">
              <thead>
                <tr role="row">
                  <th>Nombre</th>
                  <th>Provincia</th>
                  <th>País</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="tbl_localidades"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script>
  $(document).ready(()=>{
    cargarLista('');
  });
  
  function cargarLista(search){
    const data = new FormData();
    data.append('search',search);
    data.append('operacion','localidades');
    fetch(
      '/ajaxs/lugares', 
      {
        method:'POST',
        body: data
      })
    .then(r => r.json())
    .then(rpta =>{
      let localidades = rpta.localidades;
      let filas="";
      localidades.forEach(e =>{
        filas += `
          <tr>
            <td>${e.localidad}</td>
            <td>${e.provincia}</td>
            <td>${e.pais}</td>
            <td>
              <a href="localidades/${e.id_localidad}" class="a btn btn-primary btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
            </td>
          </tr>
        `;
      });
      document.querySelector('#tbl_localidades').innerHTML = filas;
      $('.table').DataTable({
        'language':lang
      });
    });
  }
</script>