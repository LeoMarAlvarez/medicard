<?php 
  include "../layouts/home_header.php" ;
  // global $url;
  // global $mysqli;
  // permisos();
  $provincia=null;
  $paises= $mysqli->query("SELECT * from paises order by nombre");
  if(isset($url[2]))
  {
    $r = $mysqli->query("select * from provincias where id_provincia=".$url[2]." limit 1");
    $provincia = $r->fetch_assoc();
  }
  if(isset($_POST['action']))
  {
    if($_POST['action']==='ok')
    {
      $mysqli->query("insert into provincias (nombre, id_pais) values ('$_POST[nombre]',$_POST[pais])");
    }else{
      $mysqli->query("update provincias set nombre='". $_POST['nombre'] . "', id_pais=".$_POST['pais']." where id_provincia=".$_POST['id']);
    }
    if($mysqli->errno != 0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/administrador/provincias');</script>";
    }else{
      echo "<script>showMsg('success','Se han guardado los cambios','/administrador/provincias');</script>";
    }
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Panel Administrativo de Provincias
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Formulario -->
  <div class="box box-default" id="formulario">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo isset($url[2])?'Editar Provincia':'Agregar Provincia'; ?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <form action="" method="post" class="text-center" autocomplete="off">
        <input type="hidden" name="action" value="<?php echo ($provincia!=null)? 'mod':'ok'; ?>">
        <input type="hidden" name="id" value="<?php echo ($provincia!=null)? $provincia['id_provincia']:''; ?>">
        <div class="row form-group">
          <label class="col-12 col-md-2 text-right">Nombre</label>
          <div class="col-12 col-md-4">
            <input type="text" name="nombre" class="form-control" required value="<?php echo ($provincia!=null)? $provincia['nombre']:'';?>">
          </div>
          <label class="col-12 col-md-2 text-right">Pais</label>
          <div class="col-12 col-md-4">
            <select name="pais" required class="form-control">
              <?php while($pais = $paises->fetch_assoc()){ ?>
                  <option value="<?php echo $pais['id_pais']?>" <?php echo ($provincia!=null)? (($provincia['id_pais']==$pais['id_pais'])? 'selected':''):''; ?> ><?php echo $pais['nombre']?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="form-group text-center">
          <input type="submit" value="Guardar" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-header with-border">
      <h3 class="box-title">Listado de Provincias</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
          <table class="table p-2 m-3 table-striped">
            <thead>
              <tr>
                <th>Provincia</th>
                <th>País</th>
                <th></th>
              </tr>
            </thead>
            <tbody id="tbl_provincias"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script>
  $(document).ready(()=>{
    cargarLista('');
  });
  
  function cargarLista(search){
    const data = new FormData();
    data.append('search',search);
    data.append('operacion','provincias');
    fetch(
      '/ajaxs/lugares', 
      {
        method:'POST',
        body: data
      })
    .then(r => r.json())
    .then(rpta =>{
      let provincias = rpta.provincias;
      let filas="";
      provincias.forEach(p =>{
        filas += `
          <tr>
            <td>${p.nombre}</td>
            <td>${p.pais}</td>
            <td>
              <a href="provincias/${p.id_provincia}" class="a btn btn-primary btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
            </td>
          </tr>
        `;
      });
      document.querySelector('#tbl_provincias').innerHTML = filas;
      $('.table').DataTable({
        'language':lang
      });
    });
  }
</script>