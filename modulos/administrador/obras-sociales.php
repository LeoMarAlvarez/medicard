<?php 
  include "../layouts/home_header.php" ;
  // global $url;
  // global $mysqli;
  // permisos();
  $obrasocial=null;
  if(isset($url[2]))
  {
    $r = $mysqli->query("select * from obras_sociales where id_obra_social=".$url[2]." limit 1");
    $obrasocial = $r->fetch_assoc();
  }
  if(isset($_POST['action']))
  {
    if($_POST['action']==='ok')
    {
      $mysqli->query("insert into obras_sociales (nombre) values ('$_POST[nombre]')");
    }else{
      $checked= isset($_POST['habilitado'])? 1:0;
      $mysqli->query("update obras_sociales set nombre='".$_POST['nombre'] . "', habilitado=". $checked . " where id_obra_social=$_POST[id]");
    }
    if($mysqli->errno != 0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/administrador/obras-sociales');</script>";
    }else{
      echo "<script>showMsg('success','Se han guardado los cambios','/administrador/obras-sociales');</script>";
    }
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Panel Administrativo de Obras Sociales
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Formulario -->
  <div class="box box-default" id="formulario">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo isset($url[2])?'Editar Obra Social':'Agregar Obra Social'; ?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <form action="" method="post" class="text-center">
        <input type="hidden" name="action" value="<?php echo ($obrasocial!=null)? 'mod':'ok'; ?>">
        <input type="hidden" name="id" value="<?php echo ($obrasocial!=null)? $obrasocial['id_obra_social']:''; ?>">
        <div class="form-group row">
          <label class="col-lg-2 text-right">Nombre</label>
          <div class="col-lg-6">
            <input type="text" name="nombre" class="form-control" value="<?php echo ($obrasocial!=null)? $obrasocial['nombre']:'';?>">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-lg-6">
            <input type="checkbox" id="habilitado" name="habilitado" <?php echo ($obrasocial!=null)? (($obrasocial['habilitado']==1)? 'checked':''):'';?>>
            <label for="habilitado"> Habilitado</label>
          </div>
        </div>
        <div class="form-group text-center">
          <input type="submit" value="Guardar" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-header with-border">
      <h3 class="box-title">Listado de Obras Sociales</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
          <table class="table p-2 m-3 table-striped">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Habilitado</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody id="tbl_obrassociales"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script>
  $(document).ready(()=>{
    cargarLista('');
  });
  
  function cargarLista(search){
    const data = new FormData();
    data.append('search',search);
    data.append('operacion','obrassociales');
    fetch(
      '/ajaxs/admin', 
      {
        method:'POST',
        body: data
      })
    .then(r => r.json())
    .then(rpta =>{
      let obrassociales = rpta.obrassociales;
      let filas="";
      obrassociales.forEach(p =>{
        if(p.habilitado==1)
        {
          estado="Activo";
          clas="text-green";
        }else{
          estado="Inactivo";
          clas="text-danger";
        }
        filas += `
          <tr>
            <td>${p.nombre}</td>
            <td><span class="${clas}">${estado}</span></td>
            <td>
              <a href="/administrador/obras-sociales/${p.id_obra_social}" class="a btn btn-primary btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
              <button class="btn btn-xs btn-danger btnBaja" onclick="eliminar('id_obra_social',${p.id_obra_social},'obras_sociales')" title="Inhabilitar"><i class="fa fa-trash"></i></button>
            </td>
          </tr>
        `;
      });
      document.querySelector('#tbl_obrassociales').innerHTML = filas;
      $('.table').DataTable({
        'language':lang
      });
    });
  }
</script>