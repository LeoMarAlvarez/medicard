<?php 
  include "../layouts/home_header.php" ;
  
  $turno=null;
  $especialidades = $mysqli->query("SELECT * from especialidades where habilitado=1 order by descripcion asc");
  $doctores= $mysqli->query("SELECT * from usuarios where habilitado=1 and rol=2 order by apellido asc");

  if(isset($url[2]))
  {
    $r = $mysqli->query("SELECT * from turnos where id_turno=".$url[2]." limit 1");
    $turno = $r->fetch_assoc();
  }
  if(isset($_POST['action']))
  {
    if($_POST['action']==='ok')
    {
      $mysqli->query("INSERT into turnos (id_dia, hora_inicio, hora_fin, id_doctor) values (".$_POST['dia'].",'".$_POST['hora_inicio']."','".$_POST['hora_fin']."',".$_SESSION['user']['id_usuario'].")");
    }else{
      $mysqli->query("UPDATE turnos set id_dia=". $_POST['dia'] . ", hora_inicio='". $_POST['hora_inicio'] . "', hora_fin='".$_POST['hora_fin']."' where id_turno=$_POST[id]");
    }

    if($mysqli->errno != 0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/secretaria/turnos');</script>";
    }else{
      echo "<script>showMsg('success','Se han guardado los cambios','/secretaria/turnos');</script>";
    }
  }
?>
<link rel="stylesheet" href="/css/modals.css">

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Solicitar Nuevo Turno
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- formulario -->
  <div class="box box-default">
    <div class="box-header with-alert">
      <h3 class="box-title"><i class="fa fa-filter"></i> Seleccione </h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <form action="" method="post" class="container_fluid">
        <div class="form-group row">
          <label class="col-lg-2">Especialidad</label>
          <div class="col-lg-10">
            <select name="especialidad" id="especialidad" class="form-control select2">
              <option value="" selected disabled >Seleccione...</option>
              <?php while($e = $especialidades->fetch_assoc()){ ?>
                <option value="<?php echo $e['id_especialidad'];?>"><?php echo $e['descripcion'];?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2">Doctor</label>
          <div class="col-lg-10">
            <select name="doctor" id="doctor" class="form-control select2" disabled>
              <option value="" selected disabled >Seleccione...</option>
              <?php while($d = $doctores->fetch_assoc()){ ?>
                <option value="<?php echo $d['id_usuario']; ?>"><?php echo $d['apellido'].' '.$d['nombre']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-lg-2 col-md-3 col-sm-12">Dias de Atención</label>
          <div class="col-lg-6 col-sm-12 text-left align-content-center align-items-center" id="dias_doctor"></div>
        </div>
        <div class="form-group row">
            <label class="col-lg-2 col-md-3 col-sm-12">Fecha</label>
            <div class="col-md-4 col-md-8 col-sm-10">
              <input type="date" disabled name="fecha" id="fecha" class="form-control">
            </div>
        </div>
      </form>
    </div>
  </div>

  <!-- Listado -->
  <div class="box box-default">
    <div class="box-header">     <h3 class="box-title">Listado de turnos disponibles</h3>
    </div>
    <div class="box-body">
      <div class="container-fluid text-center" id="turnos_body">
        
      </div>
    </div>
  </div>

  <!-- modal -->
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Turno</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="paciente" id="paciente" value="<?php echo $_SESSION['paciente']['id_paciente']; ?>">
          <div class="form-group">
            <label>Fecha</label>
            <input type="date" required disabled name="fechaTurno" id="fechaTurno">
          </div>
          <div class="form-group">
            <label>Turno</label>
            <input type="time" required disabled name="horaTurno" id="horaTurno">
          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button> -->
          <button type="submit" class="btn btn-primary" data-dismiss="modal" id="btnSendTurn">Guardar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script src="/js/functions.js"></script>
<script>
  $('#especialidad').change((e)=>{
    document.querySelector('#doctor').disabled=false;
    document.querySelector('#fecha').disabled=true;
    document.querySelector('#fecha').value=null;
    document.querySelector('#turnos_body').innerHTML='';
    showDiasDoc();
    let data = new FormData();
    data.append('operacion','medicos_por_especialidad');
    data.append('especialidad',e.target.value);
    fetch('/ajaxs/medico',{method:'POST',body:data})
    .then(r => r.json())
    .then(r =>{
      let docs = r.doctores;
      let options = `<option value="" selected disabled>Seleccione...</option>`;
      docs.forEach(d =>{
        options += `<option value="${d.id_usuario}">${d.apellido} ${d.nombre}</option>`;
      });
      document.querySelector('#doctor').innerHTML = options;
    });
  });

  let fecha = document.querySelector('#fecha');
  $('#doctor').change((e)=>{
    document.querySelector('#fecha').disabled=false;
    document.querySelector('#fecha').value=null;
    document.querySelector('#turnos_body').innerHTML='';
    showDiasDoc(e.target.value);
  });

  $('#fecha').change((e)=>{
    listarTurnos(document.querySelector('#doctor').value, e.target.value, document.querySelector('#especialidad').value, false);
    document.querySelector('#turnos_body').innerHTML='';
  });

  // Abrir Modal
  $(document).on('click', '.btnTurno', (e) => {
      let btn = e.target;
      let date = new Date();
      let fechaSelec = new Date(fecha.value+' '+btn.getAttribute('data-turno'));
      if(fechaSelec>date){
        document.querySelector('#fechaTurno').value=fecha.value;
        document.querySelector('#horaTurno').value=btn.getAttribute('data-turno');
      }else{        
        swal({text:'No se puede generar un turno de una fecha y hora pasada',icon:'error'})
        .then(()=>{
          document.location.reload();
        });
      }
      
  });

  $(document).on('click','#btnSendTurn',(e)=>{
    // e.preventDefault();
    cargarTurno(
      document.querySelector('#paciente').value,
      document.querySelector('#doctor').value,
      document.querySelector('#horaTurno').value,
      document.querySelector('#fechaTurno').value,
      null,
      document.querySelector('#especialidad').value,
      null
    );
  });
</script>