<?php 
  include "../layouts/home_header.php" ;

  if($_SESSION['recien_ingresa']){
    $_SESSION['recien_ingresa']=0;
    $cantAnt = $mysqli->query("SELECT * from antecedentes where id_paciente={$_SESSION['paciente']['id_paciente']}");
    if(!$cantAnt->num_rows>0){
      echo "<script>swal({icon:'info',text:'Recuerda que para cargar un nuevo turno, primero debes tener al menos un antecedente cargado'})</script>";
    }
  }
  
  $turnos = $mysqli->query("select t.id_turno, t.id_paciente, t.fecha, t.hora_propuesta, u.nombre, u.apellido, t.id_estado, t.id_especialidad, e.descripcion, et.descripcion as 'e_desc' from especialidades e join turnos t join usuarios u join pacientes p join estados_turno et ".
          "on e.id_especialidad=t.id_especialidad and t.id_doctor=u.id_usuario and p.id_paciente=t.id_paciente and t.id_estado=et.id_estado ".
          "where p.id_usuario = ".$_SESSION['user']['id_usuario']." and (t.id_estado=1 or t.id_estado=5) and t.fecha = '".date('Y-m-d')."' ".
                    "order by t.fecha, t.hora_propuesta asc");
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Turnos del Día
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="box box-default">
    <div class="box-header">
      <h1>Lista</h1>
    </div>
    <div class="box-body">
      <?php if($turnos->num_rows > 0){ ?>
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Día</th>
              <th>Hora</th>
              <th>Doctor</th>
              <th>Especialidad</th>
              <th>Estado</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php while($t = $turnos->fetch_assoc()){ ?>
              <tr>
                <td><?php echo dia($t['fecha']); ?></td>
                <td><?php echo $t['hora_propuesta']; ?></td>
                <td><?php echo "{$t['apellido']} {$t['nombre']}"; ?></td>
                <td><?php echo $t['descripcion']; ?></td>
                <td><span class="label <?php switch($t['id_estado']){case 1:echo 'label-info';break;case 3:echo 'label-success';break;case 4:echo 'label-danger';break;case 5:echo 'label-primary';} ?>"><?php echo $t['e_desc']?></span></td>
                <td>
                  <?php if($t['id_estado']==1){ ?>
                  <button class="btn btn-xs btn-danger" title="Cancelar turno" onclick="cancelTurn(<?php echo $t['id_turno']; ?>)"><i class="fa fa-times-circle"></i></button>
                  <?php } ?>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php }else{ ?>
        <div class="container-fluid badge badge-danger">No tienes turnos para hoy</div>
      <?php } ?>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>
<script src="/js/functions.js"></script>