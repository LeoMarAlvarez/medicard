<?php 
  include "../layouts/home_header.php" ;
  $turnos = $mysqli->query("select t.hora_inicio, t.hora_fin, t.fecha, e.descripcion, u.apellido, u.nombre, t.observacion from turnos t join especialidades e join usuarios u ".
      "on t.id_especialidad=e.id_especialidad and t.id_doctor=u.id_usuario ".
      "where t.id_paciente = {$_SESSION['paciente']['id_paciente']} and t.fecha <= '".date('Y-m-d')."' and t.id_estado=3 order by t.fecha, t.hora_propuesta desc");
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Historial de Turnos
  </h1>
</section>

<!-- Modal -->
<div id="modalObservacion" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Observación Turno </h4>
      </div>
      <div class="modal-body">
        <p id="observacion"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Volver</button>
      </div>
    </div>

  </div>
</div>

<!-- Main content -->
<section class="content">
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-header with-border">
      <h3 class="box-title">Listado</h3>
    </div>
    <div class="box-body">
      <div class="container-fluid text-center border border-info">
        <?php if($turnos->num_rows>0){ ?>
          <table class="table table-hover table-striped text-left">
            <thead>
              <tr>
                <th>Día</th>
                <th>Hora de Inicio</th>
                <th>Hora de Fin</th>
                <th>Especialidad</th>
                <th>Doctor</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php while ($turno = $turnos->fetch_assoc()){ ?>
                <tr>
                  <td><?php echo dia($turno['fecha']); ?></td>
                  <td><?php echo $turno['hora_inicio']; ?></td>
                  <td><?php echo $turno['hora_fin']; ?></td>
                  <td><?php echo $turno['descripcion']; ?></td>
                  <td><?php echo "{$turno['apellido']} {$turno['nombre']}"; ?></td>
                  <td>
                  <button 
                    class="btn btn-xs btn-info btn-observacion" 
                    data-observacion="<?php echo $turno['observacion']; ?>" 
                    data-fechaHora="<?php echo dia($turno['fecha']).' '.$turno['hora_inicio'] ?>"
                    title="Ver observación" 
                    data-toggle="modal" 
                    data-target="#modalObservacion"><i class="fa fa-info"></i> Observación</button>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        <?php }else{ ?>
          <h4 class="h4 label-default">No hay turnos pasados</h4>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>
<script>
  $(document).ready(()=>{
    $('.table').DataTable({
      'language':lang,
      "order": [[0,"desc"],[1,'desc']]
    });
  });

  $('.btn-observacion').click((e)=>{
    document.querySelector('h4.modal-title').innerText = "Turno " + e.target.getAttribute('data-fechaHora');
    document.querySelector('#observacion').innerText = e.target.getAttribute('data-observacion');
  });
</script>
<script src="/js/functions.js"></script>