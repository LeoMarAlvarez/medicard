<?php 
  include "../layouts/home_header.php" ;
  
$antecedente = $mysqli->query("SELECT a.* from antecedentes a where a.id_paciente={$_SESSION['paciente']['id_paciente']} order by a.id_antecedente desc limit 1");

if($antecedente->num_rows>0)
{
  $antecedente = $antecedente->fetch_assoc();
}else{
  $antecedente = null;
}

if(isset($_POST['peso'])){
  $dt = new DateTime();
  $fecha = $dt->format('Y-m-d');
  $hora = $dt->format('H:i:s');
  $mysqli->query("INSERT INTO antecedentes (id_paciente, fecha, hora, peso, altura, glucemia, presion_arterial_hi, presion_arterial_lo, temperatura)".
                " values({$_SESSION['paciente']['id_paciente']}, '{$fecha}','{$hora}','{$_POST['peso']}','{$_POST['altura']}','{$_POST['glucemia']}','{$_POST['presion_arterial_hi']}','{$_POST['presion_arterial_lo']}','{$_POST['temperatura']}')");

  if($mysqli->errno != 0){
    var_dump("INSERT INTO antecedentes (id_paciente, fecha, hora, peso, altura, glucemia, presion_arterial_hi, presion_arterial_lo, temperatura)".
    " values({$_SESSION['paciente']['id_paciente']}, '{$fecha}','{$hora}','{$_POST['peso']}','{$_POST['altura']}','{$_POST['glucemia']}','{$_POST['presion_arterial_hi']}','{$_POST['presion_arterial_lo']}','{$_POST['temperatura']}')");
    // echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/paciente/antecedente');</script>";
  }else{
    echo "<script>showMsg('success','Se han guardado los cambios','/paciente/antecedente');</script>";
  }
}
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Antecedentes
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="box box-default">
    <div class="box-header">
      <h2>Mi último antecedente</h2>
    </div>
    <div class="box-body">
      <form action="" method="post" class="container_fluid row">
          <!-- peso -->
          <div class="form-group col-lg-2">
            <label>Peso | <small>(Kg)</small></label>
            <input type="number" name="peso" step=".050" min="0" value="<?php echo ($antecedente!=null)? $antecedente['peso']:''; ?>" class="form-control" placeholder="Ej: 85,5">
          </div>
          <!-- altura -->
          <div class="form-group col-lg-2">
            <label>Altura | <small>(m)</small></label>
            <input type="number" name="altura" step=".05" min="1" max="2" value="<?php echo ($antecedente!=null)? $antecedente['altura']:''; ?>" class="form-control" placeholder="Ej: 1,75">
          </div>
          <!-- glucemia -->
          <div class="form-group col-lg-2">
            <label>Glucemia | <small>(mg/dL)</small></label>
            <input type="number" name="glucemia" min="10" max="300" value="<?php echo ($antecedente!=null)? $antecedente['glucemia']:''; ?>" class="form-control" placeholder="Ej: 80">
          </div>
          <!-- presion_arterial_lo -->
          <div class="form-group col-lg-2">
            <label>Presión Arterial | <small>(baja)</small></label>
            <input type="number" name="presion_arterial_lo" step="5" min="50" max="100" value="<?php echo ($antecedente!=null)? $antecedente['presion_arterial_lo']:''; ?>" class="form-control" placeholder="Ej: 80">
          </div>
          <!-- presion_arterial_hi -->
          <div class="form-group col-lg-2">
            <label>Presión Arterial | <small>(alta)</small></label>
            <input type="number" name="presion_arterial_hi" step="5" min="90" max="240" value="<?php echo ($antecedente!=null)? $antecedente['presion_arterial_hi']:''; ?>" class="form-control" placeholder="Ej: 120">
          </div>
          <!-- temperatura -->
          <div class="form-group col-lg-2">
            <label>Temperatura | <small>(°C)</small></label>
            <input type="number" name="temperatura" step=".5" min="35" max="41" value="<?php echo ($antecedente!=null)? $antecedente['temperatura']:''; ?>" class="form-control" placeholder="Ej: 37,5">
          </div>
          <div class="form-group col-lg-12 text-center">
            <br>
            <button type="submit" class="btn btn-primary">Actualizar</button>
          </div>
        </div>
      </form>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>