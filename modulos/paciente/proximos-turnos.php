<?php 
  include "../layouts/home_header.php" ;
  $turnos = $mysqli->query("select t.id_turno, t.hora_propuesta, t.id_estado, t.fecha, e.descripcion, u.apellido, u.nombre from turnos t join especialidades e join usuarios u ".
      "on t.id_especialidad=e.id_especialidad and t.id_doctor=u.id_usuario ".
      "where t.id_paciente = {$_SESSION['paciente']['id_paciente']} and t.fecha >= '".date('Y-m-d')."' and t.id_estado=1 order by t.fecha, t.hora_propuesta asc");
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Proximos Turnos
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-header with-border">
      <h3 class="box-title"></h3>
    </div>
    <div class="box-body">
      <div class="container-fluid text-center border border-info">
        <?php if($turnos->num_rows>0){ ?>
          <table class="table table-hover table-striped text-left">
            <thead>
              <tr>
                <th>Turno #</th>
                <th>Día</th>
                <th>Hora propuesta</th>
                <th>Especialidad</th>
                <th>Doctor</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1; while ($turno = $turnos->fetch_assoc()){ ?>
                <tr>
                  <th><?php echo $i; ?></th>
                  <td><?php echo dia($turno['fecha']); ?></td>
                  <td><?php echo $turno['hora_propuesta']; ?></td>
                  <td><?php echo $turno['descripcion']; ?></td>
                  <td><?php echo "{$turno['apellido']} {$turno['nombre']}"; ?></td>
                  <td>
                  <button class="btn btn-xs btn-danger" title="Cancelar turno" onclick="cancelTurn(<?php echo $turno['id_turno']; ?>)"><i class="fa fa-times-circle"></i></button>
                  </td>
                </tr>
              <?php $i++; } ?>
            </tbody>
          </table>
        <?php }else{ ?>
          <h4 class="h4 label-default">No tenés turnos cargados</h4>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>
<script>
  $(document).ready(()=>{
    $('.table').DataTable({
      'language':lang
    });
  });
</script>
<script src="/js/functions.js"></script>