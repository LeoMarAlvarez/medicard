<?php 
  include "../layouts/home_header.php" ;
  
  $pac=null;
  $obras = $mysqli->query("SELECT * from obras_sociales where habilitado=1 order by nombre asc");
  $paises = $mysqli->query("SELECT id_pais, nombre from paises order by nombre asc");
  $pacientes = $mysqli->query("select u.id_usuario ,u.apellido, u.nombre, u.dni, p.id_paciente, if(p.id_plan!='', ".
                  "(select po.descripcion from planes_os po where po.id_plan=p.id_plan),NULL) as plan, ".
                  "if(p.id_plan!='', (select os.nombre from obras_sociales os join planes_os po on os.id_obra_social=po.id_obra_social where po.id_plan=p.id_plan),NULL) as obra ".
                  "from usuarios u join pacientes p on u.id_usuario=p.id_usuario and u.habilitado=1");
  $localidades = $mysqli->query("SELECT l.id_localidad, l.nombre, p.nombre as provincia from localidades l join provincias p on l.id_provincia=p.id_provincia order by l.nombre asc");

  if(isset($url[2]))
  {
    $r = $mysqli->query("select pa.id_usuario, u.apellido, u.nombre, u.dni, u.usuario, u.password, u.habilitado, pa.id_paciente, pa.id_nacionalidad,  ".
		    "pa.id_localidad , pa.telefono, pa.fecha_nacimiento, pa.id_plan, pa.socio_obra_social, pa.direccion, pa.email, u.sexo, ".
        "if(pa.id_plan!='', (select po2.id_obra_social from planes_os po2 where po2.id_plan=pa.id_plan),null) as id_obra_social ".
        "from pacientes pa join usuarios u  ".
		    "on pa.id_usuario=u.id_usuario ".
        "where pa.id_usuario = {$url[2]}");
    $pac = $r->fetch_assoc();
    $planes = $mysqli->query("SELECT * from planes_os where habilitado=1 and id_obra_social={$pac['id_obra_social']}");
  }
  if(isset($_POST['action']))
  {
    if($_POST['action']=='ok')
    {
      $noRepes=$mysqli->query("SELECT * from usuarios u join pacientes p where u.usuario = '{$_POST['usuario']}' or u.dni='{$_POST['dni']}' or p.email='{$_POST['email']}'");
      if( $noRepes->num_rows > 0){
        echo "<script>swal({text:'Add Recuerda que el DNI, y usuario no puede repetirse. Ya existe un registro con los mismos datos',icon:'warning'});</script>";
      }else{
        /* REQUERIDOS
          ape, nom, dni, fec_nac, email, nacio,dom,sexo, user, pass
         */
        $mysqli->query("INSERT into usuarios (nombre, apellido, dni, rol, usuario, password, sexo) values('{$_POST['nombre']}','{$_POST['apellido']}','{$_POST['dni']}',4,'{$_POST['usuario']}','{$_POST['contrasenia']}', '{$_POST['sexo']}')");
        $id = $mysqli->insert_id;
        $mysqli->query("INSERT into pacientes (fecha_nacimiento, direccion, email, id_nacionalidad, id_usuario, telefono) values('{$_POST['fecha_nacimiento']}','{$_POST['domicilio']}','{$_POST['email']}',{$_POST['nacionalidad']},{$id},'{$_POST['telefono']}')");
        $idPac = $mysqli->insert_id;
        if($_POST['localidad']!=null){
          $mysqli->query("UPDATE pacientes set id_localidad={$_POST['localidad']}  where id_paciente={$idPac}");
        }
        if($_POST['plan']!=null){
          $mysqli->query("UPDATE pacientes set id_plan={$_POST['plan']}  where id_paciente={$idPac}");
        }
        if($_POST['numero_obra_social']!=null){
          $mysqli->query("UPDATE pacientes set socio_obra_social={$_POST['numero_obra_social']}  where id_paciente={$idPac}");
        }
      }
    }else{
      $noRepes=$mysqli->query("SELECT * from usuarios u join pacientes p on p.id_usuario=u.id_usuario where u.id_usuario!={$url[2]} and ((u.usuario='{$_POST['usuario']}' or u.dni='{$_POST['dni']}') or (p.email='{$_POST['email']}'))");
      if( $noRepes->num_rows > 0){
        echo "<script>swal({text:'Recuerda que el DNI, y usuario no puede repetirse. Ya existe un registro con los mismos datos',icon:'warning'});</script>";
      }else{
        $_POST['localidad'] = ($_POST['localidad']!=null)? " ,id_localidad={$_POST['localidad']} ":'';
        $_POST['plan'] = ($_POST['plan']!=null)? " ,id_plan={$_POST['plan']} ":'';
        $_POST['numero_obra_social'] = ($_POST['numero_obra_social']!=null)? " ,socio_obra_social={$_POST['numero_obra_social']} ":'';
        $mysqli->query("UPDATE pacientes set fecha_nacimiento='{$_POST['fecha_nacimiento']}', id_nacionalidad={$_POST['nacionalidad']}, direccion='{$_POST['domicilio']}', email='{$_POST['email']}', telefono='{$_POST['telefono']}' {$_POST['localidad']} {$_POST['plan']} {$_POST['numero_obra_social']} where id_usuario={$url[2]}");
        $mysqli->query("UPDATE usuarios set sexo='{$_POST['sexo']}' where id_usuario={$_POST['id']}");
      }
    }
    
    if($mysqli->errno != 0 && !$noRepes->num_rows>0){
      echo "<script>showMsg('error','Ups! no pudimos guardar los cambios. Intenta nuevamente más tarde','/secretaria/pacientes');</script>";
    }else{
      echo "<script>showMsg('success','Se han guardado los cambios','/secretaria/pacientes');</script>";
    }
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pacientes
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Formulario -->
  <div class="box box-default" id="formulario">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo isset($url[2])?'Editar Paciente':'Cargar Nuevo Paciente'; ?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <form action="" method="post" class="container-fluid row" autocomplete="off" id="form">
        <input type="hidden" name="action" value="<?php echo ($pac!=null)? 'mod':'ok'; ?>">
        <input type="hidden" name="id" value="<?php echo ($pac!=null)? $pac['id_usuario']:''; ?>">
        <div class="form-group col-lg-6">
          <label>Apellido</label>
          <input type="text" name="apellido" class="form-control" <?php echo isset($url[2])? 'readonly':''; ?> value="<?php echo isset($pac)? $pac['apellido']:'' ?>" required>
        </div>
        <div class="form-group col-lg-6">
          <label>Nombre</label>
          <input type="text" name="nombre" class="form-control" <?php echo isset($url[2])? 'readonly':''; ?> value="<?php echo isset($pac)? $pac['nombre']:'' ?>" required>
        </div>
        <div class="form-group col-lg-6">
          <label>DNI</label>
          <input type="text" name="dni" onkeyup="pressInput()" onkeypress="prevPressInput()" class="form-control" <?php echo isset($url[2])? 'readonly':''; ?> minlength="8" value="<?php echo isset($pac)? $pac['dni']:'' ?>" required>
        </div>
        <div class="form-group col-lg-6">
          <label>Teléfono</label>
          <input type="tel" class="form-control" name="telefono" value="<?php echo isset($pac)? $pac['telefono']:'' ?>">
        </div>
        <div class="form-group col-lg-6">
          <label>Fecha de Nacimiento</label>
          <input type="date" class="form-control" name="fecha_nacimiento" value="<?php echo isset($pac)? $pac['fecha_nacimiento']:'' ?>" required>
        </div>
        <div class="form-group col-lg-6">
          <label>Email</label>
          <input type="email" name="email" class="form-control" value="<?php echo isset($pac)? $pac['email']:''; ?>" required>
        </div>
        <div class="form-group">
          <hr>
        </div>
        <hr class="col-lg-11 text-center">
        <div class="form-group col-lg-3">
          <label>Nacionalidad</label>
          <select name="nacionalidad" id="" class="form-control select2" style="height:100% !important;" required>
            <option value="" selected disabled>Seleccione...</option>
            <?php while($p = $paises->fetch_assoc()){ ?>
              <option value="<?php echo $p['id_pais']?>" <?php echo isset($pac)? (($pac['id_nacionalidad']==$p['id_pais'])? 'selected':''):'' ?>><?php echo $p['nombre']?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-lg-4">
          <label>Localidad</label>
          <select name="localidad" class="form-control select2" style="height:100% !important;">
            <option value="" selected disabled>Seleccione...</option>
            <?php while($l = $localidades->fetch_assoc()){ ?>
              <option value="<?php echo $l['id_localidad'];?>" <?php echo isset($pac)? (($pac['id_localidad']==$l['id_localidad'])? 'selected':''):'';?> ><?php echo "{$l['nombre']} - {$l['provincia']}";?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-lg-5">
          <label >Domicilio</label>
          <input type="text" name="domicilio" value="<?php echo isset($pac)? $pac['direccion']:'';?>" class="form-control" required>
        </div>
        <hr class="col-lg-11 text-center">
        <div class="form-group col-lg-3">
          <label>Obra Social</label>
          <select name="obra" id="" class="form-control select2" style="height:100% !important;">
          <option value="" selected disabled>Seleccione...</option>
            <?php while($o = $obras->fetch_assoc()){ ?>
              <option value="<?php echo $o['id_obra_social']?>" <?php echo isset($pac)? (($pac['id_obra_social']===$o['id_obra_social'])? 'selected':''):'' ?>><?php echo $o['nombre']?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-lg-4">
          <label>Plan</label>
          <select name="plan" id="" class="form-control select2" style="height:100% !important;">
              <?php 
                if(isset($planes) && $planes->num_rows>0){ 
                  while($p = $planes->fetch_assoc()){
              ?>
                <option value="<?php echo $p['id_plan']; ?>" <?php echo ($p['id_plan']==$pac['id_plan'])? 'selected':''; ?>><?php echo $p['descripcion'];?></option>
              <?php }} ?>
          </select>
        </div>
        <div class="form-group col-lg-5">
          <label>Número Socio (Obra Social)</label>
          <input type="text" name="numero_obra_social" value="<?php echo isset($pac['socio_obra_social'])? $pac['socio_obra_social']:''; ?>" class="form-control">
        </div>
        <hr class="col-lg-11 text-center">
        <div class="form-group col-lg-6">
          <label>Usuario</label>
          <input type="text" name="usuario" class="form-control" <?php echo isset($url[2])? 'readonly':''; ?> value="<?php echo isset($pac['usuario'])? $pac['usuario']:'' ?>" required>
        </div>
        <div class="form-group col-lg-6">
          <label>Contraseña</label>
          <input type="password" name="contrasenia" class="form-control" <?php echo isset($url[2])? 'readonly':''; ?> value="<?php echo isset($pac['password'])? $pac['password']:'' ?>" required>
        </div>
        <div class="form-group col-lg-6">
          <label>Sexo</label>
          <select name="sexo" class="form-control" required>
              <option value=""selected disabled>Seleccione...</option>
              <option value="h" <?php echo isset($pac)? (($pac['sexo']==='h')? 'selected':''):'';?> >Hombre</option>
              <option value="m" <?php echo isset($pac)? (($pac['sexo']==='m')? 'selected':''):'';?> >Mujer</option>
          </select>
        </div>
        <div class="col-lg-12 text-center">
          <input type="submit" value="Guardar" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-header with-border">
      <h3 class="box-title">Listado de Pacientes</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
          <table class="table p-2 m-3 table-striped">
            <thead>
              <tr>
                <th>Apellido y Nombre</th>
                <th>DNI</th>
                <th>Obra Social</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody id="tbl_pacientes">
              <?php while($p = $pacientes->fetch_assoc()){ ?>
                <tr>
                  <td><?php echo "{$p['apellido']} {$p['nombre']}"; ?></td>
                  <td><?php echo $p['dni']; ?></td>
                  <td><?php echo ($p['plan']!=null)? "<span class='label label-info'>{$p['obra']} ({$p['plan']})</span>":"<span class='label label-warning'>No tiene</span>"; ?></td>
                  <td>
                    <a href="/secretaria/pacientes/<?php echo $p['id_usuario'];?>" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>

<script>
  let frm = null;
  
  $(document).ready(()=>{
    $('.table').DataTable({
      'language':lang
    });
  });

  $('select[name="obra"]').change(e=>{
    planes(e.target.value);
  });

  document.querySelector('#search').addEventListener('keyup',(e)=>{
    let nombre = e.target.value;
    cargarLista(nombre);
  });
  
  function cargarLista(search){
    const data = new FormData();
    data.append('search',search);
    data.append('operacion','listar_pacientes');
    fetch(
      '/ajaxs/pacientes', 
      {
        method:'POST',
        body: data
      })
    .then(r => r.json())
    .then(rpta =>{
      let pacientes = rpta.pacientes;
      let filas="";
      pacientes.forEach(p =>{
        filas += `
          <tr>
            <td>${p.apellido} ${p.nombre}</td>
            <td>${p.dni}</td>
            <td>${(p.descripcion!=null)? p.obra+" ("+p.descripcion+")":'(no tiene)'}</td>
            <td>
              <a href="/secretaria/pacientes/${p.id_paciente}" class="a btn btn-primary btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
            </td>
          </tr>
        `;
      });
      document.querySelector('#tbl_pacientes').innerHTML = filas;
    });
  }

  function planes(obra){
    let select = document.querySelector('select[name="plan"]');
    frm = new FormData();
    frm.append('operacion','lista_planes_os');
    frm.append('obra',obra);
    fetch('/ajaxs/generales',{method:'POST',body:frm})
    .then(r => r.json())
    .then(r =>{
      let option = `<option value="" selected disabled >Seleccione...</option>`;
      r.planes.forEach(p=>{
        option+=`
          <option value="${p.id_plan}">${p.descripcion}</option>
        `;
      });
      select.innerHTML = option;
    });

  }
</script>
<script src="/js/noLetters.js"></script>