<?php 
  include "../layouts/home_header.php" ;
  $ahora = new DateTime();
  $turnos = $mysqli->query("select t.id_turno, t.hora_propuesta, doc.nombre as 'doc_nom', doc.apellido as 'doc_ape', e.descripcion as 'e_desc', pac.nombre 'pac_nom', pac.apellido 'pac_ape', t.id_estado, esp.descripcion as especialidad,  
	        if(p.id_plan is null, 'no tiene',(select po.descripcion from planes_os po where po.id_plan=p.id_plan)) as plan, 
          if(p.id_plan is null, 'no tiene',(select os.nombre from obras_sociales os join planes_os po2 on os.id_obra_social=po2.id_obra_social where po2.id_plan=p.id_plan)) as obra
	        from turnos t join usuarios doc join pacientes p join usuarios pac join estados_turno e join especialidades esp
          on t.id_doctor=doc.id_usuario and t.id_paciente=p.id_paciente and p.id_usuario = pac.id_usuario and t.id_estado=e.id_estado and t.id_especialidad=esp.id_especialidad 
          where t.fecha='".$ahora->format('Y-m-d')."' and (t.id_estado=1 or t.id_estado=3 or t.id_estado=5 or t.id_estado=4) order by t.hora_propuesta, pac.apellido, pac.nombre, doc.apellido asc ");
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Turnos del Día
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="box box-default">
    <div class="box-header">
      <h1>Lista</h1>
    </div>
    <div class="box-body">
      <?php if($turnos->num_rows > 0){ ?>
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Hora</th>
              <th>Paciente</th>
              <th>Obra Social</th>
              <th>Especialidad</th>
              <th>Doctor</th>
              <th>Estado</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php while($t = $turnos->fetch_assoc()){ ?>
              <tr>
                <td><?php echo $t['hora_propuesta'] ?></td>
                <td><?php echo "{$t['pac_ape']} {$t['pac_nom']}"; ?></td>
                <td><?php echo "{$t['obra']} ({$t['plan']})" ?></td>
                <td><span class="label label-default"><?php echo $t['especialidad'] ?></span></td>
                <td><?php echo "{$t['doc_ape']} {$t['doc_nom']}"; ?></td>
                <td><span class="label <?php switch($t['id_estado']){case 1:echo 'label-info';break;case 3:echo 'label-success';break;case 4:echo 'label-danger';break;case 5:echo 'label-primary';} ?>"><?php echo $t['e_desc']?></span></td>
                <td>
                  <button class="btn btn-xs btn-danger" <?php echo ($t['id_estado']==3||$t['id_estado']==5||$t['id_estado']==4)? " disabled":"" ?> title="Cancelar turno" onclick="cancelTurn(<?php echo $t['id_turno']; ?>)"><i class="fa fa-times-circle"></i></button>
                  <?php if($t['id_estado']!=4){ ?>
                    <button class="btn btn-xs btn-warning" <?php echo ($t['id_estado']==3||$t['id_estado']==5)? " disabled":"" ?> title="<?php echo "Ausente {$t['pac_ape']} {$t['pac_nom']}" ?>" onclick="cambiarEstadoTurno(<?php echo $t['id_turno']?>,4)"><i class="fa fa-frown-o"></i></button>
                  <?php } ?>
                  <?php if($t['id_estado']!=5 && $t['id_estado']!=3){ ?>
                    <button class="btn btn-xs btn-primary" title="<?php echo "{$t['pac_ape']} {$t['pac_nom']} llegó" ?>" onclick="cambiarEstadoTurno(<?php echo $t['id_turno']?>,5)"><i class="fa fa-hand-paper-o"></i></button>
                  <?php } ?>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php }else{ ?>
        <div class="container-fluid badge badge-danger">No tienes turnos para hoy</div>
      <?php } ?>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>
<script src="/js/functions.js"></script>