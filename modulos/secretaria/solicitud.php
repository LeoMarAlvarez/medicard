<?php 
  include "../layouts/home_header.php" ;

  if(isset($_POST['dni'])){
    $first = $mysqli->query("SELECT * from usuarios u where u.dni='{$_POST['dni']}'");
    if($first->num_rows<1)
    {
      echo "<script>swal({text:'No existe un paciente con ese DNI.',icon:'warning'})</script>";
    }else{
      $first = $first->fetch_assoc();
      $fecha = new DateTime();
      $fecha = $fecha->format("Y-m-d H:i:s");
      $detalle= ($first['habilitado']==1)? 'Restablecimiento de contraseña':'Usuario inactivo';
      $mysqli->query("INSERT INTO solicitudes (detalle, id_usuario, fecha_solicitud) values('{$detalle}', {$first['id_usuario']}, '{$fecha}')");
      
      if($mysqli->errno!=0){
        echo "<script>swal({text:'Ups!! ocurrió un error. Intenta nuevamente más tarde',icon:'warning'});</script>";
      }else{
        $message = ($first['habilitado']==1)? 'Solicitud enviada exitosamente. Se le enviará una nueva contraseña a la brevedad':'Solicitud enviada exitosamente. Se le notificará cuando su cuenta se encuentre activa nuevamente';
        echo "<script>swal({text:'{$message}',icon:'success'});</script>";
      }
    }
  }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Solicitud de restablecimiento de contraseña / cambiar estado de cuenta del paciente
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Listado -->
  <div class="box box-default" id="lista">
    <div class="box-body">
        <form action="" method="post" class="row">
          <div class="form-group col-lg-4 col-md-6 col-sm-12">
            <input type="text" name="dni" id="" class="form-control" placeholder="DNI del paciente" required>
          </div>
          <div class="col-lg-12 form-group">
            <button type="submit" class="btn btn-primary">Enviar Solicitud</button>
          </div>
        </form>
    </div>
  </div>
</section>
<!-- /.content -->

<?php include "../layouts/home_footer.php" ?>